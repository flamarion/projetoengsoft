<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";

		/* verificações específicas da tela de avaliação */
		if(!isset($_GET["etapa"]))
			$_GET["etapa"] = 1;
		$etapa = (int) $_GET["etapa"];
		$titulo = "Avaliação - Etapa ".$etapa;
		require $head;

		require $classes."avaliacao.class.php";
		require $classes."mensagem.class.php";
		$etapas_da_avaliacao = array(
				'motivacao', 
				'amostra', 
				'historico', 
				'antropometria', 
				'perimetria', 
				'testes_finais'
			);

		$etapa_atual = $etapas_da_avaliacao[$etapa-1];

	?>
</head>

<body>
	<section class="ui centered grid">	
		<?php 
			require $header;
		?>
		<section class="black row">
			<section class="fourteen wide column">
				<?php
					if(isset($_SESSION["avaliacao"]))
						require 'avaliacao/barra_superior.php';
					else
						header("location:avaliacao_iniciar.php");
				?>
			</section>
		</section>
		<section class="row">
			<section class="eight wide column">
				<?php
					require "avaliacao_".$etapa_atual.".php";
				?>
			</section>
			<input type="hidden" value="<?php echo $_GET["etapa"] ?>" id="etapa_atual">	
		</section>
		<section class="black row">
			<section class="eight wide column">
			<?php
				if(isset($_SESSION["avaliacao"])){
					print('<a href="avaliacao_parar.php?id='.$_SESSION["avaliacao"]["id"].'" title="Parar Avaliação" class="ui left floated icon red inverted basic large button"><i class="close icon"></i>Parar</a>');
					print('<a href="avaliacao_concluir.php" class="ui right floated green large inverted basic button">Concluir Avaliação</a>');
				}
			?>
			</section>
		</section>
	</section>
	<input type="hidden" id="etapa_atual" value="<?php print($etapa_atual) ?>">

	<?php $etapas_concluidas = implode(';', $_SESSION['avaliacao']['etapas']); ?>
	<input type="hidden" id="etapas_realizadas" value="<?php print($etapas_concluidas) ?>">

	<script type="text/javascript">
		$(document).ready(function(){
			atual = $('#etapa_atual').val();
			$('#etapa_'+atual).addClass("active");

			realizadas = $('#etapas_realizadas').val().split(";");

			for(var etapa in realizadas)
				$('#etapa_'+realizadas[etapa]).addClass("completed");
		})
	</script>
</body>

</html>