<!DOCTYPE html>
<html>
<head>
	<?php
		$titulo = "Cadastro";
		require "conf.php";
		require "conexao.php";
		require $head;
	?>
</head>

<body class="background">
	<section class="ui centered grid">
		<?php require $header ?>
		<section class="row">
			<section class="eight wide column">
			<section class="ui segment">
				<h2 class="ui header">
					<i class="add user icon"></i>
					<section class="content">
						Cadastro de Usuário
						<section class="sub header">
							Preencha o formulário abaixo
						</section>
					</section>
				</h2>
				<?php 
					if(count($_POST) == 0)
					{
						require $models."sexo.class.php";
						require $models."estado_civil.class.php";
						require $models."etnia.class.php";
						require $forms."cadastros/usuario.html";
					}
					else
					{
						require $models."usuario.class.php";
						require $models."pessoa.class.php";
						require $models."contato.class.php";
						require $classes."mensagem.class.php";

						$pessoa = new Pessoa();
						$pessoa->setDados(
								$_POST["nome"],
								$_POST["sobrenome"],
								$_POST["data_nascimento"],
								$_POST["cpf"],
								$_POST["rg"],
								$_POST["etnia"],
								$_POST["estado_civil"],
								$_POST["sexo"]
							);

						if($pessoa->inserir($con)){
							$id_pessoa = $pessoa->getId();
							$usuario = new Usuario();
							$usuario->setLogin($_POST["login"]);
							$usuario->setSenha($_POST["senha"]);
							$usuario->setPessoa($id_pessoa);
							
							$contato = new Contato();
							$contato->setDados($pessoa->getId(), $_POST["email"], null, null);
							$contato->inserir($con);

							if($usuario->inserir($con)){
								$mensagem = new Mensagem(1, "Usuário registrado!");
								$mensagem->addMensagem("Parabéns ".$_POST["nome"].", você já pode fazer login no sistema!");
								$mensagem->getMensagem();
								print('<a href="index.php" class="ui fluid positive huge button"><i class="home icon"></i>Início</a>');
							}
						}
					}
				?>
				</section>
			</section>
		</section>
	</section>
</body>

</html>