<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Gerenciar Avaliações";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header; 
			require $menu;
		?>
		<section class="row"> <!-- content !-->
			<section class="ten wide column">
				<section class="ui segment">
					<h3 class="ui header">
						<i class="warning sign icon"></i>
						<section class="content">
							Avaliações realizadas
							<section class="sub header">Gerencie-as</section>
						</section>
					</h3>

					<section class="ui large divided list">
					<?php
						$id_usuario = $_SESSION["usuario"]["id"];

						require $models."avaliacao.class.php";
						require $classes."mensagem.class.php";

						$avaliacoes = new Avaliacao();
						$lista_avaliacoes = $avaliacoes->listar($con, $id_usuario, null);

						if($lista_avaliacoes->num_rows > 0){
							while($avaliacao = $lista_avaliacoes->fetch_assoc()){
						?>
						<section class="item">
							<section class="left floated content">
								<h4 class="header">
									<span class="ui circular blue label"><?php print($avaliacao["id_avaliacao"]); ?></span>
									Avaliação
								</h4>
								Cliente: 
								<strong>
									<a href="cliente.php?id=<?php print($avaliacao["id_cliente"]); ?>"> <?php print($avaliacao["nome"]." ".$avaliacao["sobrenome"]); ?></a>
								</strong>
								<?php
									if($avaliacao["data_hora"] != null)
										print(', em '.$avaliacao["data_hora"]);
								?>
							</section>
							<section class="right floated content">
								<?php
									if($avaliacao["id_status_avaliacao_id"] == 1)
										print('<label class="ui orange icon basic label"><i class="warning icon"></i> Incompleta</label>');
									else
										print('<label class="ui green icon circular button"><i class="check icon"></i></label>');
								?>
								<a href="#">
									<button class="ui green basic button"><i class="unhide icon"></i>Visualizar</button>
								</a>
								<a href="avaliacao_reiniciar.php?id=<?php print($avaliacao["id_avaliacao"]) ?>">
									<button class="ui green icon button"><i class="play icon"></i></button>
								</a>
							</section>
						</section>
						<?php
							}
						}
						else{
							$mensagem = new Mensagem(2, "Sem avaliações!");
							$mensagem->addMensagem("Não foram realizadas avaliações!");
							$mensagem->getMensagem();
						}
					?>
					</section>
				</section>
			</section>
		</section>
		<?php
			require $footer;
		?>
</body>

</html>
