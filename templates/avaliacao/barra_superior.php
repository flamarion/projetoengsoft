
<section class="ui four top attached steps">
	<section class="step" id="step_1">
		<i class="heartbeat icon"></i>
		<section class="content">
			<article class="title">Amostra de Saúde</article>
			<article class="description">Etapa Prévia</article>
		</section>
	</section>
	<section class="step" id="step_2">
		<i class="edit icon"></i>
		<section class="content">
			<article class="title">Histórico de Saúde</article>
			<article class="description">Saúde do Cliente</article>
		</section>
	</section>
	<section class="step" id="step_3">
		<i class="edit icon"></i>
		<section class="content">
			<article class="title">Antropometria</article>
			<article class="description">Medidas antropométricas</article>
		</section>
	</section>
	<section class="step" id="step_4">
		<i class="edit icon"></i>
		<section class="content">
			<article class="title">Perimetria</article>
			<article class="description">Medidas perimétricas</article>
		</section>
	</section>
	<section class="step" id="step_5">
		<i class="doctor icon"></i>
		<section class="content">
			<article class="title">Testes Finais</article>
			<article class="description">Última etapa</article>
		</section>
	</section>
</section>
