<?php 
	if(isset($titulo) && isset($appnome)){
		echo "<title>".$appnome." - ".$titulo."</title>";
	}
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link type="text/css" rel="stylesheet" href="static/semantic/dist/semantic.css">
<link type="text/css" rel="stylesheet" href="static/estilo/theme.css">

<script type="text/javascript" src="static/jquery/jquery.js"></script>
<script type="text/javascript" src="static/semantic/dist/semantic.js"></script>
<script type="text/javascript" src="static/js/script.js"></script>