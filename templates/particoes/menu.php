<section class="preto row">
	<section class="twelve wide column">
		<nav class="ui inverted menu">

			<a href="painel.php" class="item">
				<i class="home icon"></i>
				Início
			</a>
			
			<section class="ui dropdown item">
				<i class="users icon"></i>
				Clientes
				<i class="dropdown icon"></i>
				<nav class="inverted menu">
					<a href="clientes.php" class="item"><i class="users icon"></i>Lista</a>!-->
					<a href="cliente_adicionar.php" class="item"><i class="add user icon"></i>Adicionar</a>
				</nav>
			</section>
			
			<section class="ui dropdown item">
				<i class="edit icon"></i>
				Avaliações
				<i class="dropdown icon"></i>
				<nav class="menu">
					<a href="avaliacoes.php" class="item"><i class="archive icon"></i>Avaliações</a>
				</nav>
			</section>

			<section class="right menu">
				
				<a href="logout.php">
					<button class="ui red label item">
						Sair
					</button>
				</a>

			</section>
		</nav>
	</section>
</section>