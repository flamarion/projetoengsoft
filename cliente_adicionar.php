<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Adicionar Cliente";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header; 
			require $menu;
		?>
		<section class="row"> <!-- content !-->
			<section class="ten wide column">


				<section class="ui segment">
					<h2 class="ui header">
						<i class="add user icon"></i>
						<section class="content">
							Cadastro de Cliente
							<section class="sub header">
								Preencha o formulário abaixo com os dados do cliente!
							</section>
						</section>
					</h2>
				
					<?php

						require $models."sexo.class.php";
						require $models."estado_civil.class.php";
						require $models."etnia.class.php";
						require $models."uf.class.php";

						if(count($_POST)>0){

							require $models."pessoa.class.php";
							require $models."cliente.class.php";
							require $models."vinculacao.class.php";
							require $models."endereco.class.php";
							require $models."contato.class.php";
							require $classes."mensagem.class.php";

							$pessoa = new Pessoa();
							$pessoa->setDados(
								$_POST["nome"],
								$_POST["sobrenome"],
								$_POST["data_nascimento"],
								$_POST["cpf"],
								$_POST["rg"],
								$_POST["etnia"],
								$_POST["estado_civil"],
								$_POST["sexo"]
							);
							
							if($pessoa->inserir($con)){
								
								$cliente = new Cliente();
								$cliente->setPessoa($pessoa->getId());

								if($cliente->inserir($con)){

									$vinculacao = new Vinculacao($cliente->getId(), $_SESSION["usuario"]["id"]);

									if($vinculacao->registrar($con)){
										$endereco = new Endereco();
										$endereco->setDados(
												$pessoa->getId(),
												$_POST["rua"],
												$_POST["numero"],
												$_POST["cep"],
												$_POST["bairro"],
												$_POST["cidade"],
												$_POST["uf"],
												$_POST["observacoes"]
											);
										$contato = new Contato();
										$contato->setDados(
												$pessoa->getId(),
												$_POST["email"],
												$_POST["telefone"],
												$_POST["celular"]
											);

										$mensagem = new Mensagem(1, "Cliente registrado!");
										if($endereco->inserir($con))
											$mensagem->addMensagem('Endereço cadastrado!');
										if($contato->inserir($con))
											$mensagem->addMensagem('Contato cadastrado!');
										$mensagem->addMensagem('Cliente '.$_POST["nome"].' está registrado no sistema!');
										$mensagem->getMensagem();
									}
								}
							}
							else{
								$mensagem = new Mensagem(0, "Falhou!");
								$mensagem->addMensagem("Houve um erro: não foi possível registrar o cliente!");
								$mensagem->getMensagem();
							}
						}

						require $forms."cadastros/cliente.html";
					?>
					
				</section>
			</section>
		</section>
		<?php
			require $footer;
		?>
	</section>
</body>

</html>
