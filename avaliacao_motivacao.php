<section class="ui blue segment">
	
	<h2 class="ui header">
		<i class="idea icon"></i>
		<section class="content">
			Motivações à prática de atividade física
			<section class="sub header">
				Para cada possível motivação, defina o grau de importância dela para sua prática!
			</section>
		</section>
	</h2>

	<?php
	require $models."grau_importancia.class.php";
	require $models."motivacao.class.php";
	require $models."motivacao_atividade_fisica.class.php";

	$array_motivacoes  = array();
	$array_graus = array();
	$id_avaliacao = $_SESSION["avaliacao"]["id"];
	
	$motivacao_atividade_fisica = new Motivacao_atividade_fisica($id_avaliacao, null, null);

	$g = new Grau_importancia();
	$graus_importancia = $g->listar($con);
	while($grau = $graus_importancia->fetch_assoc())
		array_push($array_graus, $grau);

	$m = new Motivacao();
	$motivacoes = $m->listar($con);
	while($motivacao = $motivacoes->fetch_assoc())
		array_push($array_motivacoes, $motivacao);

	if(count($_POST) > 0){
		if($motivacao_atividade_fisica->estaPreenchido($con)){
			
		}
		else{
			foreach ($array_motivacoes as $m) {
				$id_motivacao = $m["id_motivacao"];
				$id_grau = $_POST["motivacao_".$id_motivacao];
				
				$nova_motivacao = new Motivacao_atividade_fisica($id_avaliacao, $id_motivacao, $id_grau);
				$nova_motivacao->inserir($con);
			}

			array_push($_SESSION["avaliacao"]["etapas"], $etapa_atual);
		}
	}

	if($motivacao_atividade_fisica->estaPreenchido($con)){
		$busca_motivacoes = $motivacao_atividade_fisica->buscar($con);
		print("<input type='hidden' id='preenchimento' value='1'>");
		while($dados_motivacao = $busca_motivacoes->fetch_assoc())
			print("<input type='hidden' id='valor_".$dados_motivacao["id_motivacao_id"]."' value='".$dados_motivacao["id_grau_importancia_id"]."'>");
	}
	?>
	<form method="POST" action="#">
		<table class="ui definition table">
			<thead>
				<tr>
					<th></th>
					<th>
						Grau de importância
					</th>
				</tr>
			</thead>
			<tbody>
		<?php
		foreach ($array_motivacoes as $motivacao) {
			?>
				<tr>
					<td>
						<?php print($motivacao["desc_motivacao"]); ?>
					</td>
					<td>
						<select name="motivacao_<?php print($motivacao["id_motivacao"]); ?>" class="ui fluid dropdown">
							<?php
								foreach ($array_graus as $grau)
									print('<option value="'.$grau["id_grau_importancia"].'">'.$grau["desc_grau_importancia"].'</option>');
							?>
						</select>
					</td>
				</tr>
			<?php
		}
	?>
			</tbody>
		</table>

		<input type="submit" class="ui right floated submit green button" value="Salvar">
	</form>

</section>

<script type="text/javascript" src="static/js/motivacao_atividade_fisica.js"></script>