<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Iniciar Avaliação";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";

		if(isset($_SESSION["avaliacao"]))
			header("location:avaliacao.php");
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header;
			require $menu;
		?>
		<section class="row">
			<section class="eight wide column">
			<?php
				$id_usuario = $_SESSION["usuario"]["id"];

				if(count($_POST)>0){
					require $models.'avaliacao.class.php';
					require $classes.'mensagem.class.php';
					$vinculacao = $_POST["vinculacao"];

					$avaliacao = new Avaliacao();
					$avaliacao->setDados($vinculacao, 1, '0000-00-00');

					if($avaliacao->inserir($con)){
					 	$_SESSION["avaliacao"] = array(
					 		"id" => $avaliacao->getId(),
					 		"etapas" => array()
					 	);
					 	header("location:avaliacao.php");
					}
					else{
						$mensagem = new Mensagem(0, "Avaliação não pôde iniciar!");
						$mensagem->addMensagem("Ocorreu um erro durante a inicialização da avaliação!");
						$mensagem->getMensagem();
						print('<a href="clientes.php" class="ui fluid blue button">Lista de clientes</a>');
					}
				}
				else{
					if(isset($_GET["cliente"])){
						require $models."vinculacao.class.php";
						$id_cliente = $_GET["cliente"];
						$vinculacao = new Vinculacao($id_cliente, $id_usuario);

						$vinculo = $vinculacao->buscar($con);
						if($vinculo != null){
							$sql = "SELECT id_vinculacao, id_cliente, nome FROM vinculacao v INNER JOIN cliente c ON (v.id_cliente_id = c.id_cliente) INNER JOIN pessoa p ON (c.id_pessoa_id = p.id_pessoa) WHERE id_cliente = $id_cliente";
							
							if($busca = $con->query($sql)){
							 	$cliente = $busca->fetch_assoc();
							 	require $forms."inicia_avaliacao.html";
							}
						}
						else
							header("location: clientes.php");
					}
					else
						header("location: clientes.php");
				}

				$con->close();
			?>
			</section>
		</section>
	</section>
</body>

</html>
