<!DOCTYPE html>
<html>

<head>
	<?php
		session_start();
		$titulo = "Login";
		require "conf.php";
		require "funcoes/conexao.php";
		require "particoes/head.php";
	?>
</head>

<body>

	<section class="ui centered grid">
		<?php require "particoes/header.php" ?>
		<section class="ui row">
			<section class="three wide column">
			<?php
				require "classes/log.class.php";
				require "classes/mensagem.class.php";

				if(count($_POST)>0){
					
					$log = new LogIn($_POST["login"], $_POST["senha"]);
					if($log->getLogin($con)){
						$dados = $log->getDadosUsuario();
						unset($dados["senha"]);
						$_SESSION["usuario"] = $dados;
						header("location:painel.php");
					}else{
						$mensagem = new Mensagem(0, "Erro!");
						$mensagem->addMensagem($log->getErro());
						$mensagem->getMensagem();
					}
				}

				require($templates."formularios/form_login.php");
				mysqli_close($con);
			?>
			</section>
		</section>
	</section>
</body>

</html>
