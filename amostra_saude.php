<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Amostra de Saúde";
		require "conf.php";
		require "funcoes/conexao.php";
		require "particoes/head.php";
		require "funcoes/verifica_sessao.php";
	?>
	<script type="text/javascript" src="static/js/motivacao.js"></script>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require "particoes/header.php"; 
			require "particoes/menu.php";
		?>
		<section class="row"> <!-- content !-->
			<section class="eight wide column">
				<h2 class="ui header">
					<i class="add user icon"></i>
					<div class="content">
						Amostra de Saúde
						<article class="sub header">A amostra de saúde é fundamental para a análise de avaliações futuras</article>
					</div>
				</h2>

				<section class="ui segment">
					<form method="POST" action="" class="ui form">			

						<article class="ui dividing header">
							Quais os motivos que levaram à atividade física?
						</article>

						<section class="ui large labels" id="motivacoes">
							<a class="ui blue label">
								Prevenção
							</a>

							<a class="ui blue label">
								Indicação Médica
							</a>

							<a class="ui blue label">
								Estética
							</a>

							<a class="ui blue label">
								Lazer
							</a>

							<a class="ui blue label">
								Condicionamento Físico
							</a>

							<a class="ui blue label">
								Ganho de Massa Magra
							</a>

							<a class="ui blue label">
								Perda de Gordura
							</a>
						</section>

						<section class="ui large ordered middle aligned divided list">
							<section class="disabled item" id="motivacao0">
								<input type="hidden" name="motivacao0" value="">
								<span>Nenhuma</span>
							</section>

							<section class="disabled item" id="motivacao1">
								<input type="hidden" name="motivacao1" value="">
								<span>Nenhuma</span>
							</section>

							<section class="disabled item" id="motivacao2">
								<input type="hidden" name="motivacao2" value="">
								<span>Nenhuma</span>
							</section>

							<section class="disabled item" id="motivacao3">
								<input type="hidden" name="motivacao3" value="">
								<span>Nenhuma</span>
							</section>

							<section class="disabled item" id="motivacao4">
								<input type="hidden" name="motivacao4" value="">
								<span>Nenhuma</span>
							</section>

							<section class="disabled item" id="motivacao5">
								<input type="hidden" name="motivacao5" value="">
								<span>Nenhuma</span>
							</section>

							<section class="disabled item" id="motivacao6">
								<input type="hidden" name="motivacao6" value="">
								<span>Nenhuma</span>
							</section>
						</section>

						<article class="ui dividing header">Sobre a prática de atividade física:</article>
								
						<table class="ui compact celled definition table">
							<tr>
								<td class="collapsing">
									<section class="ui toggle checkbox">
										<input type="checkbox" name="  ">
										<label></label>
									</section>
								</td>
								<td>
									<!-- Motivo !-->
									Tem praticado atividade física regularmente?
								</td>
							</tr>
						</table>

						<p class="text">* Se <strong>sim</strong> para a pergunta anterior, continue a preencher esta sessão do formulário</p>

						<section class="two fields">
							<section class="field">
								<label>Qual(is) atividade(s)?</label>
								<textarea name="atividades_praticadas"></textarea>
							</section>

							<section class="field">
								<section class="field">
									<label>Há quanto tempo pratica?</label>
									<section class="ui right labeled input">
										<input type="text" name="tempo_pratica" style="text-align: center;" maxlength="2">
										<select class="ui dropdown label">
											<option value="dias">Dias</option>
											<option value="semanas">Semanas</option>
											<option value="meses">Meses</option>
											<option value="anos">Anos</option>
										</select>
									</section>
								</section>

								<section class="field">
									<label>Qual a frequência?</label>
									<section class="ui right labeled input">
										<input type="text" name="frequencia"  style="text-align: center;" maxlength="1">
										<label class="ui label">
											Dias na Semana
										</label>
									</section>
								</section>

								<section class="field">
									<label>Com qual frequência pretende vir à academia?</label>
									<section class="ui right labeled input">
										<input type="text" name="frequencia_academia"  style="text-align: center;" maxlength="1">
										<label class="ui label">
											Dias na Semana
										</label>
									</section>
								</section>
							</section>
						</section>
						
						<section class="field">
							<label>Observações</label>
							<textarea name="observacoes"></textarea>
						</section>
					</form>
				</section>
			</section>
		</section>
		<?php
			require "particoes/footer.php";
		?>
</body>

</html>
