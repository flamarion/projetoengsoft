<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Perfil Cliente";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header; 
			require $menu;
		
			$id_usuario = $_SESSION["usuario"]["id"];
			$id_cliente = $_GET["id"];

			if($id_cliente == null){
				header("location:clientes.php");
			}

			require $models."vinculacao.class.php";
			require $classes."mensagem.class.php";
			$vinculacao = new Vinculacao($id_cliente, $id_usuario);
			$vinc = $vinculacao->buscar($con);
			
			if($vinc != null){
				require $models."pessoa.class.php";
				require $models."cliente.class.php";
				require $models."endereco.class.php";
				require $models."contato.class.php";
				require $models."avaliacao.class.php";
				require $control."data.php";

				$cliente = new Cliente();
				$cliente->setId($id_cliente);
				$cli = $cliente->buscar($con);

				if($cli != null){
					$endereco = new Endereco();
					$endereco->setPessoa($cli["id_pessoa_id"]);
					$end = $endereco->buscar($con);

					$contato = new Contato();
					$contato->setPessoa($cli["id_pessoa_id"]);
					$cont = $contato->buscar($con);

					$avaliacao = new Avaliacao();
					$avaliacoes = $avaliacao->listar($con, $id_usuario, $id_cliente);

					require $templates.'perfil_cliente.html';
				}
				else{
					header("location:painel.php");
				}
			}
			else{
				print('<section class="row">
					<section class="four wide column">');
				$mensagem = new Mensagem(0, "Usuário inválido!");
				$mensagem->addMensagem("Este usuário não existe!");
				$mensagem->getMensagem();
				print('
					<section class="ui two buttons">
						<a href="cliente_adicionar.php" class="ui green button">
							Adicionar cliente
						</a>
						<a href="clientes.php" class="ui teal button">
							Lista de clientes
						</a>
					</section>');
				print('</section></section>');
			}
		?>
		<?php
			require $footer;
		?>
</body>

</html>
