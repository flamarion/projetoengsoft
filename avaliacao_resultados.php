<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Gerenciar Avaliações";
		require "conf.php";
		require "particoes/head.php";
		require "funcoes/verifica_sessao.php";

		if(isset($_GET["id"]))
			$id_avaliacao = $_GET["id"];
		else
			header("location:avaliacoes.php");
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require "particoes/header.php"; 
			require "particoes/menu.php";
		?>
		<section class="row"> <!-- content !-->
			<section class="ten wide column">
				<?php
					require "funcoes/conexao.php";
					require "classes/mensagem.class.php";

					$busca_avaliacao = $con->query("SELECT * FROM avaliacao WHERE id_avaliacao = $id_avaliacao");
					if($busca_avaliacao->num_rows > 0){
						require "classes/motivacao.class.php";
						require "classes/amostra.class.php";
						require "classes/historico.class.php";
						require "classes/antropometria.class.php";
						require "classes/perimetria.class.php";

						$avaliacao = $busca_avaliacao->fetch_assoc();
						$id_cliente = $avaliacao["id_cliente_id"];
						$id_usuario = $avaliacao["id_usuario_id"];

						$__motivacao = new MotivacaoAtividadeFisica($id_avaliacao);
						$__amostra = new AmostraDeSaude($id_avaliacao);
						$__historico = new HistoricoDeSaude($id_avaliacao);
						$__antropometria = new Antropometria($id_avaliacao);
						$__perimetria = new Perimetria($id_avaliacao);

						if($__motivacao->estaFeito($con))
							$motivacoes = $__motivacao->buscar_registros($con);
						if($__amostra->estaFeito($con))
							$amostra_de_saude = $__amostra->buscar_registros($con);
						if($__historico->estaFeito($con))
							$historico_de_saude = $__historico->buscar_registros($con);
						if($__antropometria->estaFeito($con))
							$antropometria = $__antropometria->buscar_registros($con);
						if($__perimetria->estaFeito($con))
							$perimetria = $__perimetria->buscar_registros($con);

						require "avaliacao/resultado.php";

					}
					else{
						$mensagem = new Mensagem(0, "Erro!");
						$mensagem->addMensagem("Esta avaliação não existe!");
						$mensagem->getMensagem();
					}
					$con->close();
				?>
			</section>
		</section>
		<?php
			require "particoes/footer.php";
		?>
</body>

</html>
