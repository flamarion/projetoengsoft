<?php
	
	require $models."amostra_saude.class.php";
	$id_avaliacao = $_SESSION["avaliacao"]["id"];

	$verificacao = new Amostra_saude();
	$verificacao->setAvaliacao($id_avaliacao);

	$preenchido = $verificacao->estaPreenchido($con);

	if(count($_POST) > 0){

		$tempo_pratica = $_POST["tempo_pratica_atividade_fisica"]." ".$_POST["contagem_de_tempo"];

		$amostra = new Amostra_saude();
		$amostra->setDados(
				$id_avaliacao,
				$_POST["descricao_atividades"],
				$tempo_pratica,
				$_POST["frequencia_atividade_fisica"],
				$_POST["frequencia_academia"],
				$_POST["observacoes"]
			);

		if($preenchido){
			if($amostra->atualizar($con))
				print('<h2>Amostra de saúde atualizada!</h2>');
		}
		else{
			if($amostra->inserir($con)){
				print('<h2>Amostra de saúde registrada!</h2>');
				array_push($_SESSION["avaliacao"]["etapas"], 'amostra');
			}
			else
				print('<h2>Não foi possível registrar a amostra de saúde!</h2>');
		}

	}

?>
<section class="ui red segment">
	<section class="eight wide column">
		<h2 class="ui header">
			<i class="treatment icon"></i>
			<div class="content">
				Amostra de Saúde
				<article class="sub header">A amostra de saúde é fundamental para a análises futuras</article>
			</div>
		</h2>
		<?php
			
			if(isset($dados)){
				foreach ($dados as $field => $valor) {
					print('<input type="hidden" id="'.$field.'" value="'.$valor.'">');	
				}
			}
		?>
		<form method="POST" action="avaliacao.php?etapa=<?php echo $etapa ?>" class="ui form">

			<article class="ui dividing header">Sobre a prática de atividade física:</article>

			<section class="eight wide field">
				<label>Há quanto tempo pratica?</label>
				<section class="ui right labeled input">
					<input type="text" name="tempo_pratica_atividade_fisica" style="text-align: right;" maxlength="2">
					<select class="ui dropdown" name="contagem_de_tempo" id="contagem_de_tempo">
						<option value="dias">Dias</option>
						<option value="semanas">Semanas</option>
						<option value="meses">Meses</option>
						<option value="anos">Anos</option>
					</select>
				</section>
			</section>

			<section class="field">
				<label>Descreva as atividades</label>
				<textarea name="descricao_atividades"></textarea>
			</section>
			
			<section class="two fields">
				<section class="field">
					<label>Frequência da prática de Atividade Física</label>
					<section class="ui right labeled input">
						<input name="frequencia_atividade_fisica" maxlength="1">
						<label class="ui label">Dias na Semana</label>
					</section>
				</section>
				
				<section class="field">
					<label>Frequência na academia</label>
					<section class="ui right labeled input">
						<input name="frequencia_academia" maxlength="1">
						<label class="ui label">Dias na Semana</label>
					</section>
				</section>
			</section>

			<section class="field">
				<label>Observações</label>
				<textarea name="observacoes"></textarea>
			</section>

			<input type="reset" class="ui left floated orange button" value="Limpar">
			<input type="submit" class="ui right floated green submit button" value="Salvar">
		</form>
	</section>
</section>

<script type="text/javascript" src="static/js/amostra_saude.js"></script>
<?php
if($preenchido){
	$amostra_atual = $verificacao->buscar($con)->fetch_assoc();

	print("<input type='hidden' id='preenchimento' value='1'>");
	print("<input type='hidden' id='valor_tempo_pratica_atividades' value='".$amostra_atual["tempo_pratica_atividades"]."'>");
	print("<input type='hidden' id='valor_descricao_atividades' value='".$amostra_atual["desc_atividades_fisicas"]."'>");
	print("<input type='hidden' id='valor_frequencia_semanal_pratica' value='".$amostra_atual["frequencia_semanal_pratica"]."'>");
	print("<input type='hidden' id='valor_frequencia_academia' value='".$amostra_atual["frequencia_academia"]."'>");
	print("<input type='hidden' id='valor_observacoes' value='".$amostra_atual["observacoes"]."'>");
}
?>