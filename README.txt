@ Felipe Flamarion da Conceição
@ 12-08-2016
@ IFC - Campus Araquari

# Aplicação de avaliações físicas

~ antes de executar é preciso carregar o Banco de dados

/__bd__.sql 

Contém toda a estrutura e alguns dados

#1: Crie um base de dados no MySQL com o nome "ava_2"
Importe o arquivo "__bd__.sql"

#ou 2: Crie uma base de dados no MySQL com o "nome_da_base" (nome desejado)
Importe o arquivo "__bd__.sql"
No arquivo "/conexao.php" configure os parâmetros conforme:

- O nome escolhido para a base de dados
- Usuario
- Senha
- Porta


