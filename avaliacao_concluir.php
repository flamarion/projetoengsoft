<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		require "conf.php";
		require "funcoes/verifica_sessao.php";
		require "particoes/head.php";
	?>
</head>

<body>
	<section class="ui centered grid">	
		<?php 
			require 'particoes/header.php';
		?>
		<section class="row">
			<section class="six wide column">
				<?php
					if(!isset($_SESSION["avaliacao"]))
						header("location:avaliacao_iniciar.php");

					if(!isset($_GET["concluir"]))
					{
				?>
					<section class="ui center aligned segment">
						<h1 class="ui icon header">
							<i class="announcement icon"></i>
							<section class="content">
								Concluir avaliação?
								<section class="sub header">Você completou todas as etapas?</section>
							</section>
						</h1>
					</section>
					
					<a href="avaliacao.php">
						<button class="ui left floated left labeled icon negative big submit button">
							<i class="close icon"></i>
							Cancelar
						</button>
					</a>
					<a href="avaliacao_concluir.php?concluir=true">
						<button class="ui right floated left labeled icon positive big submit button">
							<i class="check icon"></i>
							Concluir
						</button>
					</a>

				<?php 
					}
					else{
						$encerrar = $_GET["concluir"];
						if($encerrar == "true"){
							require "classes/avaliacao.class.php";
							require "classes/mensagem.class.php";
							require "funcoes/conexao.php";
							$avaliacao = new Avaliacao();
							if($avaliacao->concluir($con, $_SESSION["avaliacao"]["id"]))
							{
								$con->close();
								header("location:painel.php");
							}
							else
							{
								$mensagem = new Mensagem(0, "Erro");
								$mensagem->addMensagem("Algo inesperado aconteceu! Por favor, tente encerrar novamente!");
								$mensagem->getMensagem();
								print('<a href="avaliacao_concluir.php" class="ui fluid button">Tentar Novamente</a>');
							}
							$con->close();
						}
						else
							header("location:avaliacao.php");
					}
				?>
			</section>
		</section>
	</section>
</body>

</html>

<script type="text/javascript">
	$(document).ready(function(){
	})
</script>