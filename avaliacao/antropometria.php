<?php
	require "classes/antropometria.class.php";
	require "funcoes/conexao.php";

	$antropometria = new Antropometria($id_avaliacao);

	if(count($_POST) == 0)
	{
		if($antropometria->estaFeito($con))
			$registros = $antropometria->buscar_registros($con);
		require "formularios/antropometria.html";	
	}
	else
	{
		print("<h1>Resultado da antropometria</h1>");
		$antropometria->setDados($_POST);
		if($antropometria->estaFeito($con)){
			if($antropometria->atualizar($con))
				print("Antropometria atualizada com sucesso!");
			else
				print("Não foi possível atualizar a antropometria!");
		}
		else{
			if($antropometria->inserir($con))
				print("Antropometria registrada com sucesso!");
			else
				print("Não foi possível registrar a antropometria!");
		}
	}
	$con->close();
?>