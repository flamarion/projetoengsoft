<?php

require "classes/amostra.class.php";
require "funcoes/conexao.php";

$amostra_de_saude = new AmostraDeSaude($id_avaliacao);

if(count($_POST) == 0)
{
	if($amostra_de_saude->estaFeito($con)){
		$consulta = $amostra_de_saude->buscar_registros($con);
		while($dados=$consulta->fetch_assoc()){
			break;
		}
	}
	require "formularios/amostra.html";
}
else
{
	$amostra_de_saude->setDados($_POST);
	try{
		if($amostra_de_saude->estaFeito($con)){
			if($amostra_de_saude->atualizar($con)){
				print("Registros atualizados com sucesso!");
			}
		}
		else{
			if($amostra_de_saude->inserir($con)){
				print("Amostra de saúde registrada!");
				print('<a href="avaliacao.php?etapa='.($etapa+1).'">Histórico de Saúde</a>');
			}
			else{
				print('Não foi possível registrar a amostra de saúde!');
			}
		}
	}
	catch(Exception $e){}
}

$con->close();