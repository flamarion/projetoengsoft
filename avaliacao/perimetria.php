<?php
	
require "classes/perimetria.class.php";
require "funcoes/conexao.php";

$perimetria = new Perimetria($id_avaliacao);

if(count($_POST) == 0){
	if($perimetria->estaFeito($con))
		$registros = $perimetria->buscar_registros($con);
	require "formularios/perimetria.html";
}
else{
	$perimetria->setDados($_POST);

	if($perimetria->estaFeito($con)){
		if($perimetria->atualizar($con))
			print("Perimetria atualizada!");
		else
			print("Não foi possível atualizar perimetria!");
	}
	else{
		if($perimetria->inserir($con))
			print("Perimetria registrada!");
		else
			print("Não foi possível registrar perimetria!");
	}
}

$con->close();