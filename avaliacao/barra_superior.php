
<section class="ui six top attached ordered steps">
	<section class="step" id="etapa_motivacao">
		<section class="content">
			<a href="avaliacao.php?etapa=1">
				<article class="title">Motivações</article>
			</a>
			<article class="description">Etapa Prévia</article>
		</section>
	</section>
	<section class="step" id="etapa_amostra">
		<section class="content">
			<a href="avaliacao.php?etapa=2">
				<article class="title">Amostra de Saúde</article>
			</a>
			<article class="description">Etapa Prévia</article>
		</section>
	</section>
	<section class="step" id="etapa_historico">
		<section class="content">
			<a href="avaliacao.php?etapa=3">
				<article class="title">Histórico de Saúde</article>
			</a>
			<article class="description">Saúde do Cliente</article>
		</section>
	</section>
	<section class="step" id="etapa_antropometria">
		<section class="content">
			<a href="avaliacao.php?etapa=4">
				<article class="title">Antropometria</article>
			</a>
			<article class="description">Protocolo antropométrico</article>
		</section>
	</section>
	<section class="step" id="etapa_perimetria">
		<section class="content">
			<a href="avaliacao.php?etapa=5">
				<article class="title">Perimetria</article>
			</a>
			<article class="description">Medidas perimétricas</article>
		</section>
	</section>
	<section class="step" id="etapa_testes_finais">
		<section class="content">
			<a href="avaliacao.php?etapa=6">
				<article class="title">Testes Finais</article>
			</a>
			<article class="description">Última etapa</article>
		</section>
	</section>
</section>

