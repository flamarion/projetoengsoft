<?php

require "classes/historico.class.php";
$historico_de_saude = new HistoricoDeSaude($id_avaliacao);

if(count($_POST) == 0)
{
	if($historico_de_saude->estaFeito()){
		$registros = $historico_de_saude->buscar_registros();
		//print_r($registros);
	}
	require "formularios/historico.html";
}
else
{
	//print_r($_POST);
	$historico_de_saude->setDados($_POST);

	if($historico_de_saude->estaFeito()){
		if($historico_de_saude->atualizar()){
			print("Histórico de saúde atualizado!");
		}
	}
	else{
		if($historico_de_saude->inserir()){
			print("Histórico de saúde registrado!");
		}
	}
}
