<?php

require "classes/motivacao.class.php";
require "funcoes/conexao.php";
$motivacao_atividade_fisica = new MotivacaoAtividadeFisica($id_avaliacao);

if(count($_POST) == 0) /* verifica o método de acesso ao template */
{	
	/* se for por GET */
	if($motivacao_atividade_fisica->estaFeito($con)) /* Confere se este formulário já foi preenchido para esta avaliação */
		$registros = $motivacao_atividade_fisica->buscar_registros($con); /* Se sim: busca registros atualizados */
	require "formularios/motivacao.html";
}
else
{	
	$motivacoes = $con->query("SELECT id_motivacao FROM motivacao");

	if($motivacao_atividade_fisica->estaFeito($con))
	{
		print("<h3>Atualizando...</h3>");
		try{
			while($motivacao = $motivacoes->fetch_assoc())
			{
				$id_motivacao = $motivacao["id_motivacao"];
				$id_grau = $_POST["grau_motivacao_".$id_motivacao];
				$motivacao_atividade_fisica->atualizar($con, $id_motivacao, $id_grau);
			}
			print("<p>Dados atualizados!</p>");
		}
		catch(Exception $e){print($e);}
	}
	else{
		print("<h3>Inserindo...</h3>");
		try{
			while($motivacao = $motivacoes->fetch_assoc())
			{
				$id_motivacao = $motivacao["id_motivacao"];
				$id_grau = $_POST["grau_motivacao_".$id_motivacao];
				$motivacao_atividade_fisica->inserir($con, $id_motivacao, $id_grau);
			}
			print("<p>Dados atualizados!</p>");
		}
		catch(Exception $e){print($e);}
	}
}
$con->close();