<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Parar avaliação";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header; 
			require $menu;
		?>
		<section class="row">
			<section class="six wide column">
			<?php

				if(isset($_GET["id"]) == false)
					header("location:avaliacao.php");

				require $classes."mensagem.class.php";
				unset($_SESSION["avaliacao"]);
				$mensagem = new Mensagem(2, "Avaliação parada!");
				$mensagem->addMensagem("A avaliação não está ativa, mas ainda não foi concluída!");
				$mensagem->addMensagem("Lembre-se que você pode continuar a avaliação quando quiser!");
				$mensagem->getMensagem();
				print('<a href="painel.php">
						<button class="ui fluid button">Painel</button>
					</a>');
			?>
			</section>
		</section>
		<?php
			require $footer;
		?>
</body>

</html>
