<?php

class Usuario{

	private $nome_entidade = "usuario";
	private $tabela = "usuario";

	private $id;
	private $pessoa;
	private $login;
	private $senha;

	function __construct(){}

	function setDados($login, $senha){
		$this->login = $login;
		$this->senha = $this->criptografarSenha($senha);
	}

	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setLogin($login){
		$this->login = $login;
	}

	function getLogin(){
		return $this->login;
	}

	function setSenha($senha){
		$this->senha = $this->criptografarSenha($senha);
	}

	function getSenha(){
		return $this->senha;
	}

	function setPessoa($pessoa){
		$this->pessoa = $pessoa;
	}

	function getPessoa(){
		return $this->pessoa;
	}

	function inserir($con){
		$sql = "INSERT INTO ".$this->tabela." (id_pessoa_id, login, senha)";
		$sql .= "VALUES ($this->pessoa, '$this->login', '$this->senha')";
		
		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else{
			print("<p><ERRO - USUÁRIO> ".$con->error."</p>");
		}
		return false;
	}

	function buscar($con){}

	function validar($con){}

	function listar($con){}

	function visualizar(){
		print("<p>Pessoa: ".$this->pessoa."</p>");
		print("<p>Login: ".$this->login."</p>");
		print("<p>Senha: ".$this->senha."</p>");
	}

	function criptografarSenha($senha){
		return password_hash($senha, PASSWORD_BCRYPT);
	}


}