<?php

class Dobra_cutanea{

	private $nome_entidade = "dobra_cutanea";
	private $tabela = "dobra_cutanea";

	private $id;
	private $desc;
	private $sigla;

	function __construct(){}

	function setDados($id, $descricao, $sigla){
		$this->id = $id;
		$this->desc = $descricao;
		$this->sigla = $sigla;
	}

	function inserir($con){
		$sql = "INSERT INTO $this->tabela (id_dobra_cutanea, desc_dobra_cutanea, sigla_dobra)";
		$sql .= "VALUES ($this->id, '$this->desc', '$this->sigla')";

		if($con->query($sql))
			print("<p>".ucwords($this->nome_entidade)." ".$this->desc." inserido com sucesso!</p>");
		else
			print("<p> Erro:".$con->error."</p>");
	}

	function listar($con){
		$sql = "SELECT * FROM ".$this->tabela;
		try{
			$busca = $con->query($sql);
			if($busca->num_rows > 0)
				return $busca;
		}
		catch(Exception $e){}
		return null;
	}

}