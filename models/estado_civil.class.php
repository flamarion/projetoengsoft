<?php

class Estado_civil{

	private $nome_entidade = "estado_civil";
	private $tabela = "estado_civil";

	private $id;
	private $desc;

	function __construct(){}

	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setDesc($desc){
		$this->desc = $desc;
	}

	function getDesc(){
		return $this->desc;
	}

	function inserir($con){
		$sql = "INSERT INTO ".$this->tabela." (id_estado_civil, desc_estado_civil) VALUES (".$this->id.", '".$this->desc."')";
		if($con->query($sql))
			print("<p>".ucwords($this->nome_entidade)." ".$this->desc." inserido com sucesso!</p>");
		else
			print("<p> Erro:".$con->error."</p>");
	}

	function buscar($con){}

	function validar($con){}

	function listar($con){
		$sql = "SELECT * FROM ".$this->tabela;
		try{
			$busca = $con->query($sql);
			if($busca->num_rows > 0)
				return $busca;
		}
		catch(Exception $e){}
		return null;
	}

}