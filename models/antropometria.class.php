<?php

class Antropometria{

	private $tabela = "antropometria";

	private $id;
	private $avaliacao;
	private $altura;
	private $peso;

	private $massa_magra;
	private $massa_gorda;
	private $densidade_corporal;
	private $percentual_gordura;
	private $peso_max_recomendavel;
	private $objetivo_emagrecimento;

	private $observacoes;

	function setDados($avaliacao, $altura, $peso){
		$this->avaliacao = $avaliacao;
		$this->altura = $altura;
		$this->peso = $peso;
	}

	function setId($id){
		$this->id = $id;
	}

	function setAvaliacao($avaliacao){
		$this->avaliacao = $avaliacao;
	}

	function inserir($con){
		$sql = "INSERT INTO $this->tabela";
		$sql .= "(id_avaliacao_id, altura, peso) ";
		$sql .= "VALUES($this->avaliacao, $this->altura, $this->peso) ";

		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else
			print("<h2>$con->error()</h2>");
		return false;
	}

	function estaPreenchido($con){
		$sql = "SELECT * FROM $this->tabela WHERE id_avaliacao_id = $this->avaliacao";
		if($busca = $con->query($sql)){
			if($busca->num_rows > 0)
				return true;
			else
				return false;
		}
		else{
			print("<h3>$con->error</h3>");
		}
	}

	function buscar($con){
		$sql = "SELECT * FROM $this->tabela WHERE id_avaliacao_id = $this->avaliacao";
		if($busca = $con->query($sql))
			return $busca;
		else
			return null;
	}

	function registrarMedidaDobra($con, $dobra_cutanea, $valor){
		if($this->id != null){
			$sql = "INSERT INTO medida_dobra VALUES($this->id, $dobra_cutanea, $valor)";
			$con->query($sql);
		}
	}

}