<?php

class Historico_saude{

	private $tabela = "historico_saude";

	private $id;
	private $avaliacao;

	private $consumo_alcool;
	private $consumo_fumo;
	private $problema_cardiovascular;
	private $problema_cardiovascular_familiar;
	private $problema_ortopedico;
	private $consumo_medicamentos;
	private $problema_menstrual;

	private $pressao_arterial_maxima;
	private $pressao_arterial_minima;
	private $frequencia_cardiaca;

	private $observacoes;


	function __construct(){}

	function setDados($avaliacao, $pressao_maxima, $pressao_minima, $frequencia, $observacoes){
		$this->avaliacao = $avaliacao;
		$this->pressao_arterial_maxima = $pressao_maxima;
		$this->pressao_arterial_minima = $pressao_minima;
		$this->frequencia_cardiaca = $frequencia;
		$this->observacoes = $observacoes;
	}

	function setQuestionario($q1, $q2, $q3, $q4, $q5, $q6, $q7){
		$this->consumo_alcool = $q1;
		$this->consumo_fumo = $q2;
		$this->problema_cardiovascular = $q3;
		$this->problema_cardiovascular_familiar = $q4;
		$this->problema_ortopedico = $q5;
		$this->consumo_medicamentos = $q6;
		$this->problema_menstrual = $q7;
	}

	function setAvaliacao($avaliacao){}

	function inserir($con){
		$sql = "INSERT INTO $this->tabela (
				id_avaliacao_id,
				consumo_alcool,
				consumo_fumo,
				problema_cardio,
				problema_cardio_familiar,
				lesao,
				consumo_medicamento,
				problema_menstrual,
				pressao_arterial_min,
				pressao_arterial_max,
				frequencia_cardiaca,
				observacoes
			)";

		$sql .= " VALUES (
				$this->avaliacao,
				$this->consumo_alcool,
				$this->consumo_fumo,
				$this->problema_cardiovascular,
				$this->problema_cardiovascular_familiar,
				$this->problema_ortopedico,
				$this->consumo_medicamentos,
				$this->problema_menstrual,
				$this->pressao_arterial_minima,
				$this->pressao_arterial_maxima,
				$this->frequencia_cardiaca,
				'$this->observacoes'
			)";
	
		print($sql);

	}

	function buscar($con){}

	function atualizar($con){}

}