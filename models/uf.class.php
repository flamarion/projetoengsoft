<?php

class UF{

	private $nome_entidade = "uf";
	private $tabela = "uf";
	
	private $sigla;
	private $desc;

	function __construct(){}

	function setSigla($sigla){
		$this->sigla = $sigla;
	}

	function setDados($sigla, $desc){
		$this->sigla = $sigla;
		$this->desc = $desc;
	}

	function getSigla(){
		return $this->sigla;
	}

	function setDesc($desc){
		$this->desc = $desc;
	}

	function getDesc(){
		return $this->desc;
	}

	function inserir($con){
		$sql = "INSERT INTO ".$this->tabela." (sigla_uf, desc_uf) VALUES('".$this->sigla."','".$this->desc."')";
		if($con->query($sql))
			print("<p>".ucwords($this->nome_entidade)." ".$this->desc." inserido com sucesso!</p>");
		else
			print("<p> Erro:".$con->error."</p>");
	}

	function buscar($con, $sigla){}

	function validar($con){}

	function listar($con){
		$sql = "SELECT * FROM ".$this->tabela;
		try{
			$busca = $con->query($sql);
			if($busca->num_rows > 0)
				return $busca;
		}
		catch(Exception $e){}
		return null;
	}
}