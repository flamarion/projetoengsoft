<?php

class Vinculacao{

	private $nome_entidade = "vinculacao";
	private $tabela = "vinculacao";

	private $id;
	private $cliente;
	private $usuario;
	private $data;

	function __construct($cliente, $usuario){
		$this->cliente = $cliente;
		$this->usuario = $usuario;
	}

	function registrar($con){
		$sql = "INSERT INTO ".$this->tabela." (id_cliente_id, id_usuario_id) VALUES ($this->cliente, $this->usuario)";

		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else{
			print('<h4>{Erro - Vinculacao} '.$con->error.'</h4>');
		}
		return false;
	}

	function buscar($con){
		$sql = "SELECT * FROM vinculacao WHERE id_cliente_id = $this->cliente AND id_usuario_id = $this->usuario";
		if($busca = $con->query($sql)){
			if($busca->num_rows == 1)
				return $busca->fetch_assoc();
		}
		return null;
	}

}