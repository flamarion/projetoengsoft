<?php

class Motivacao_atividade_fisica{

	private $nome_entidade = "motivacao_atividade_fisica";
	private $tabela = "motivacao_atividade_fisica";

	private $avaliacao;
	private $motivacao;
	private $grau;

	function __construct($avaliacao, $motivacao, $grau){
		$this->avaliacao = $avaliacao;
		$this->motivacao = $motivacao;
		$this->grau = $grau;
	}

	function inserir($con){
		$sql = "INSERT INTO $this->tabela (id_avaliacao_id, id_motivacao_id, id_grau_importancia_id)";
		$sql .= " VALUES($this->avaliacao, $this->motivacao, $this->grau)";

		if(!$con->query($sql))
			print('<p>Não foi possível registrar a motivação: '.$con->error.'</p>');
	}

	function estaPreenchido($con){
		$sql = "SELECT * FROM $this->tabela WHERE id_avaliacao_id = $this->avaliacao";
		if($busca = $con->query($sql))
			if($busca->num_rows > 0)
				return true;
			else
				return false;
		else
			print("<h3>$con->error</h3>");
	}

	function buscar($con){
		$sql = "SELECT * FROM $this->tabela WHERE id_avaliacao_id = $this->avaliacao";
		if($busca = $con->query($sql))
			return $busca;
		else
			return null;
	}

}