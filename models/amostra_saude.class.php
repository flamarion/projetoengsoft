<?php

class Amostra_saude{

	private $tabela = "amostra_saude";

	private $id;
	private $avaliacao;
	private $desc_atividades_fisicas;
	private $tempo_pratica_atividades;
	private $frequencia_semanal_pratica;
	private $frequencia_academia;
	private $observacoes;

	function __construct(){}

	function setDados($avaliacao, $desc, $tempo, $frequencia_pratica, $frequencia_academia, $observacoes){
		$this->avaliacao = $avaliacao;
		$this->desc_atividades_fisicas = $desc;
		$this->tempo_pratica_atividades = $tempo;
		$this->frequencia_semanal_pratica = $frequencia_pratica;
		$this->frequencia_academia = $frequencia_academia;
		$this->observacoes = $observacoes;
	}

	function setAvaliacao($avaliacao){
		$this->avaliacao = $avaliacao;
	}

	function inserir($con){
		$sql = "INSERT INTO $this->tabela (id_avaliacao_id, desc_atividades_fisicas, tempo_pratica_atividades, frequencia_semanal_pratica, frequencia_academia, observacoes)";
		$sql .= " VALUES($this->avaliacao, '$this->desc_atividades_fisicas', '$this->tempo_pratica_atividades', $this->frequencia_semanal_pratica, $this->frequencia_academia, '$this->observacoes')";

		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else
			print('<h3>$con->error</h3>');
		return false;
	}

	function estaPreenchido($con){
		$sql = "SELECT * FROM $this->tabela WHERE id_avaliacao_id = $this->avaliacao";
		if($busca = $con->query($sql)){
			if($busca->num_rows > 0)
				return true;
			else
				return false;
		}
		else{
			print("<h3>$con->error</h3>");
		}
	}

	function buscar($con){
		$sql = "SELECT * FROM $this->tabela WHERE id_avaliacao_id = $this->avaliacao LIMIT 0,1";
		if($busca = $con->query($sql))
			if($busca->num_rows == 1)
				return $busca;
		return null;
	}

	function atualizar($con){
		$sql = "UPDATE $this->tabela SET ";
		$sql .= "desc_atividades_fisicas = '$this->desc_atividades_fisicas', ";
		$sql .= "tempo_pratica_atividades = '$this->tempo_pratica_atividades', ";
		$sql .= "frequencia_semanal_pratica = $this->frequencia_semanal_pratica, ";
		$sql .= "frequencia_academia = $this->frequencia_academia, ";
		$sql .= "observacoes = '$this->observacoes' ";
		$sql .= "WHERE id_avaliacao_id = $this->avaliacao";

		if($con->query($sql))
			return true;
		else
			print("<h2>$con->error</h2>");
		return false;
	}

}