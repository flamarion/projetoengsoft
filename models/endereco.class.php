<?php

class Endereco{

	private $nome_entidade = "endereco";
	private $tabela = "endereco";

	private $pessoa;
	private $rua;
	private $numero;
	private $cep;
	private $bairro;
	private $cidade;
	private $uf;
	private $observacoes;

	function __construct(){}

	function setDados($pessoa, $rua, $numero, $cep, $bairro, $cidade, $uf, $observacoes){
		$this->pessoa = $pessoa;
		$this->rua = $rua;
		$this->numero = $numero;
		$this->cep = $cep;
		$this->bairro = $bairro;
		$this->cidade = $cidade;
		$this->uf = $uf;
		$this->observacoes = $observacoes;
	}

	function setPessoa($pessoa){
		$this->pessoa = $pessoa;
	}

	function inserir($con){
		$sql = "INSERT INTO ".$this->tabela." (id_pessoa_id, cep, rua, numero, bairro, cidade, uf, observacoes)";
		$sql .= " VALUES($this->pessoa, '$this->cep', '$this->rua', '$this->numero', '$this->bairro', '$this->cidade', '$this->uf', '$this->observacoes')";
		
		if($con->query($sql))
			return true;
		else
			print('<h4>{Erro - Endereço} '.$con->error.'</h4>');
		return false;
	}

	function buscar($con){
		$sql = "SELECT * FROM $this->tabela INNER JOIN uf ON (uf = sigla_uf) WHERE id_pessoa_id = $this->pessoa";

		if($busca = $con->query($sql))
			if($busca->num_rows == 1)
				return $busca->fetch_assoc();
		else
			print('<h4>{Erro - Endereço} '.$con->error.'</h4>');
		return false;
	}

}