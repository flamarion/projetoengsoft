<?php

class Avaliacao{

	private $nome_entidade = "avaliacao";
	private $tabela = "avaliacao";

	private $id;
	private $vinculacao;
	private $status;
	private $data;

	function __construct(){}

	function setDados($vinculacao, $status, $data){
		$this->vinculacao = $vinculacao;
		$this->status = $status;
		$this->data = $data;
	}

	function setId($id){
		$this->id = $id;
	}

	function setStatus($status){
		$this->status = $status;
	}

	function setVinculacao($vinculacao){
		$this->vinculacao = $vinculacao;
	}

	function getId(){
		return $this->id;
	}

	function inserir($con){
		$sql = "INSERT INTO $this->tabela (id_status_avaliacao_id, id_vinculacao_id) VALUES ($this->status, $this->vinculacao)";
		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else
			print('<h3>{Erro - Iniciação de avaliação} '.$con->error.'</h3>');
		return false;
	}

	function getAvaliacoesIncompletas($con, $usuario){
		$sql = "SELECT count(id_avaliacao) as quantidade FROM $this->tabela INNER JOIN vinculacao ON (id_vinculacao_id = id_vinculacao)
		WHERE id_usuario_id = $usuario";

		if($busca = $con->query($sql))
			return $busca->fetch_assoc();
		else
			print('<h3>{Erro - Verificação} '.$con->error.'</h3>');
		return null;
	}

	function listar($con, $usuario, $cliente){
		$sql = "SELECT id_cliente, nome, sobrenome, avaliacao.* FROM avaliacao 
		INNER JOIN vinculacao ON (id_vinculacao = id_vinculacao_id) 
		INNER JOIN cliente ON (id_cliente_id = id_cliente)
		INNER JOIN pessoa ON (id_pessoa_id = id_pessoa)
		WHERE id_usuario_id = $usuario";
		if($cliente!=null)
			$sql .= " AND id_cliente_id = $cliente";
		$sql .= " ORDER BY id_avaliacao DESC LIMIT 0,10";

		if($busca = $con->query($sql))
			return $busca;
		else
			print('<h3><Erro na listagem de avaliação> $con->error</h3>');
		return null;
	}

	function buscar($con){
		$sql = "SELECT id_avaliacao FROM avaliacao WHERE id_avaliacao = $this->id";
		if($busca = $con->query($sql))
			if($busca->num_rows == 1)
				return $busca->fetch_assoc();
		return null;
	}

}