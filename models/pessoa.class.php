<?php

class Pessoa{

	private $nome_entidade = "pessoa";
	private $tabela = "pessoa";

	private $id;
	private $nome;
	private $sobrenome;
	private $data_nascimento;
	private $cpf;
	private $rg;
	private $etnia;
	private $estado_civil;
	private $sexo;

	function __construct(){}

	function setDados($nome, $sobrenome, $data_nascimento, $cpf, $rg, $etnia, $estado_civil, $sexo){
		$this->nome = $nome;
		$this->sobrenome = $sobrenome;
		$this->data_nascimento = $this->dataFormatoHifen($data_nascimento);
		$this->cpf = $cpf;
		$this->rg = $rg;
		$this->etnia = $etnia;
		$this->estado_civil = $estado_civil;
		$this->sexo = $sexo;
	}

	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setNome($nome){
		$this->nome = $nome;
	}

	function getNome(){
		return $this->nome;
	}

	function setSobrenome($sobrenome){
		$this->sobrenome = $sobrenome;
	}

	function geSobrenome(){
		return $this->sobrenome;
	}

	function setDataNascimento($data_nascimento){
		$this->data_nascimento = $this->dataFormatoHifen($data_nascimento);
	}

	function getDataNascimento(){
		return $this->data_nascimento;
	}

	function setCpf($cpf){
		$this->cpf = $cpf;
	}

	function getCpf(){
		return $this->cpf;
	}

	function setRg($rg){
		$this->rg = $rg;
	}

	function getRg(){
		return $this->rg;
	}

	function setEtnia($etnia){
		$this->etnia = $etnia;
	}

	function getEtnia(){
		return $this->etnia;
	}

	function setEstado_civil($estado_civil){
		$this->estado_civil = $estado_civil;
	}

	function getEstado_civil(){
		return $this->estado_civil;
	}

	function setSexo($sexo){
		$this->sexo = $sexo;
	}

	function getSexo(){
		return $this->sexo;
	}

	function inserir($con){
		$sql = "INSERT INTO ".$this->tabela." (nome, sobrenome, data_nascimento, cpf, rg, id_etnia_id, id_estado_civil_id, id_sexo_id)";
		$sql .= "VALUES ('$this->nome', '$this->sobrenome', '$this->data_nascimento', '$this->cpf', '$this->rg', $this->etnia, $this->estado_civil, $this->sexo)";

		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else{
			print("<p><ERRO - PESSOA> ".$con->error."</p>");
		}
		return false;
	}

	function buscar($con){}

	function validar($con){}

	function listar($con){}

	function dataFormatoHifen($data){
		$array_data = explode("/", $data);
		$dia = $array_data[0];
		$mes = $array_data[1];
		$ano = $array_data[2];
		return "$ano-$mes-$dia";
	}

	function dataFormatoBarra($data){
		$array_data = explode("-", $data);
		$dia = $array_data[2];
		$mes = $array_data[1];
		$ano = $array_data[0];
		return "$dia/$mes/$ano";
	}

	function deletar($con){}

	function visualizar(){
		print('<h2>Pessoa: '.$this->nome.'</h2>');
		print('<strong>Nome</strong> '.$this->nome.'</br>');
		print('<strong>Sobrenome</strong> '.$this->sobrenome.'</br>');
		print('<strong>Data nascimento</strong> '.$this->data_nascimento.'</br>');
		print('<strong>CPF</strong> '.$this->cpf.'</br>');
		print('<strong>RG</strong> '.$this->rg.'</br>');
		print('<strong>Sexo</strong> '.$this->sexo.'</br>');
		print('<strong>Estado Civil</strong> '.$this->estado_civil.'</br>');
		print('<strong>Etnia</strong> '.$this->etnia.'</br>');
		print('<hr>');

	}

}