<?php

class Cliente{

	private $nome_entidade = "cliente";
	private $tabela = "cliente";

	private $id;
	private $pessoa;

	function __construct(){}

	function setId($id){
		$this->id = $id;
	}

	function getId(){
		return $this->id;
	}

	function setPessoa($pessoa){
		$this->pessoa = $pessoa;
	}

	function getPessoa(){
		return $this->id;
	}

	function inserir($con){
		$sql = "INSERT INTO ".$this->tabela." (id_pessoa_id) VALUES ($this->pessoa)";

		if($con->query($sql)){
			$this->id = $con->insert_id;
			return true;
		}
		else{
			print('<h4>{Erro - Cliente} '.$con->error.'</h4>');
		}
		return false;
	}

	function buscar($con){
		$sql = "SELECT * FROM cliente 
		INNER JOIN pessoa ON (cliente.id_pessoa_id = pessoa.id_pessoa)
		INNER JOIN sexo ON (pessoa.id_sexo_id = sexo.id_sexo)
		INNER JOIN estado_civil ON (pessoa.id_estado_civil_id = estado_civil.id_estado_civil)
		INNER JOIN etnia ON (pessoa.id_etnia_id = etnia.id_etnia)
		WHERE id_cliente = $this->id";

		if($busca = $con->query($sql))
			if($busca->num_rows > 0)
				return $busca->fetch_assoc();
		else
			print('<h4>{Erro - Cliente} '.$con->error.'</h4>');
		return null;
	}

	function validar(){}

	function listar($con, $usuario, $filtro, $inicio, $limite){

		$sql = "SELECT id_cliente as id, nome, sobrenome, email, data_nascimento, desc_sexo FROM
		cliente c INNER JOIN pessoa p ON (c.id_pessoa_id = p.id_pessoa)
		INNER JOIN contato ct ON (ct.id_pessoa_id = p.id_pessoa)
		INNER JOIN vinculacao v ON (v.id_cliente_id = id_cliente)
		INNER JOIN usuario u ON (v.id_usuario_id = u.id_usuario)
		INNER JOIN sexo s ON (s.id_sexo = p.id_sexo_id)
		WHERE id_usuario = $usuario
		AND (nome LIKE '%$filtro%' OR sobrenome LIKE '%$filtro%')
		ORDER BY nome ASC, sobrenome ASC";

		if(isset($inicio) && isset($limite))
			$sql .= " LIMIT $inicio, $limite";

		if($busca = $con->query($sql))
			return $busca;
		else
			print('<h4>{Erro - listagem de clientes} '.$con->error.'</h4>');
		return null;
	}

}