<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Painel";
		require("conf.php");
		require($funcoes."banco.php");
		$con = conecta();
		require($funcoes."log.php");
		require($templates."bases/head.php");

		verifica();
	?>
</head>

<body>
	<div class="ui centered grid">
		<div class="black row">
			<?php
				require($templates."bases/topo_painel.php");
			?>
		</div>
		<div class="row">
			<section class="twelve wide column">

				<section class="ui four top attached steps">
					<section class="step" id="step_1">
						<i class="heartbeat icon"></i>
						<section class="content">
							<article class="title">Histórico de Saúde</article>
						</section>
					</section>
					<section class="step" id="step_2">
						<i class="edit icon"></i>
						<section class="content">
							<article class="title">Antropometria</article>
						</section>
					</section>
					<section class="step" id="step_3">
						<i class="edit icon"></i>
						<section class="content">
							<article class="title">Perimetria</article>
						</section>
					</section>
					<section class="step" id="step_4">
						<i class="doctor icon"></i>
						<section class="content">
							<article class="title">Testes Finais</article>
						</section>
					</section>
				</section>

				<script type="text/javascript" src="static/js/script_etapas.js"></script>
			</section>
		</div>
		<div class="row">
			<section class="eight wide column">
			<?php
				require $templates.'formularios/form_historico_saude.php';

				if(isset($_GET["etapa"])){
					echo '<input type="hidden" value="'.$_GET["etapa"].'" id="numero_etapa">';
				}

				mysqli_close($con); 
			?>
			</section>
		</div>
	</div>
</body>

</html>
