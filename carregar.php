<h1>Carregando dados essenciais</h1>

<?php

require "conf.php";
require "conexao.php";

/* ##################################
	SEXOS
################################### */

print("<h2>Sexos</h2>");

require $models."sexo.class.php";
$m = new Sexo();
$m->setId(1);
$m->setDesc("Masculino");
$m->inserir($con);

$con->query("INSERT INTO sexo VALUES (1, 'masculino')");

$f = new Sexo();
$f->setId(2);
$f->setDesc("Feminino");
$f->inserir($con);

$sexos = new Sexo();
$lista_sexos = $sexos->listar($con);
print("<ul>");
while($sexo = $lista_sexos->fetch_assoc()){
	print("<li>".$sexo["id_sexo"].": ".$sexo["desc_sexo"]."</li>");
}
print("</ul>");

/* ##################################
	ESTADOS CIVIS
################################### */

print("<h2>Estados Civis</h2>");
require $models."estado_civil.class.php";
$solteiro = new Estado_civil();
$solteiro->setId(1);
$solteiro->setDesc("Solteiro");
$solteiro->inserir($con);

$casado = new Estado_civil();
$casado->setId(2);
$casado->setDesc("Casado");
$casado->inserir($con);

$divorciado = new Estado_civil();
$divorciado->setId(3);
$divorciado->setDesc("Divorciado");
$divorciado->inserir($con);

$viuvo = new Estado_civil();
$viuvo->setId(4);
$viuvo->setDesc("Viúvo");
$viuvo->inserir($con);

$estados_civis = new Estado_civil();
$lista_estados_civis = $estados_civis->listar($con);
print("<ul>");
while($estado_civil = $lista_estados_civis->fetch_assoc()){
	print("<li>".$estado_civil["id_estado_civil"].": ".$estado_civil["desc_estado_civil"]."</li>");
}
print("</ul>");

/* ##################################
	ETNIAS
################################### */

print("<h2>Etnias</h2>");
require $models."etnia.class.php";
$p = new Etnia();
$p->setDados(1, "Pardo");
$p->inserir($con);

$n = new Etnia();
$n->setDados(2, "Negro");
$n->inserir($con);

$b = new Etnia();
$b->setDados(3, "Branco");
$b->inserir($con);

$o = new Etnia();
$o->setDados(4, "Oriental");
$o->inserir($con);

$i = new Etnia();
$i->setDados(5, "Indígena");
$i->inserir($con);

$h = new Etnia();
$h->setDados(6, "Hispânico");
$h->inserir($con);

$etnias = new Etnia();
$lista_etnias = $etnias->listar($con);
print("<ul>");
while($etnia = $lista_etnias->fetch_assoc()){
	print("<li>".$etnia["id_etnia"].": ".$etnia["desc_etnia"]."</li>");
}
print("</ul>");

/* ##################################
	STATUS DE AVALIAÇÃO
################################### */

print("<h2>Status Avaliação</h2>");
require $models."status_avaliacao.class.php";

$a = new Status_avaliacao();
$a->setDados(1, "Aberta");
$a->inserir($con);

$f = new Status_avaliacao();
$f->setDados(2, "Fechada");
$f->inserir($con);

$status_avaliacao = new Status_avaliacao();
$lista_status = $status_avaliacao->listar($con);
print("<ul>");
while($status = $lista_status->fetch_assoc()){
	print("<li>".$status["id_status_avaliacao"].": ".$status["desc_status_avaliacao"]."</li>");
}
print("</ul>");

/* ##################################
	ESTADOS - UF
################################### */
print("<h2>Estados - UF</h2>");
require $models."uf.class.php";

$rs = new UF();
$rs->setDados("RS", "Rio Grande do Sul");
$rs->inserir($con);

$sc = new UF();
$sc->setDados("SC", "Santa Catarina");
$sc->inserir($con);

$pr = new UF();
$pr->setDados("PR", "Paraná");
$pr->inserir($con);

$sp = new UF();
$sp->setDados("SP", "São Paulo");
$sp->inserir($con);

$estados = new UF();
$lista_estados = $estados->listar($con);
print("<ul>");
while($uf = $lista_estados->fetch_assoc()){
	print("<li>".$uf["sigla_uf"].": ".$uf["desc_uf"]."</li>");
}
print("</ul>");

/* ##################################
	MOTIVAÇÕES À ATIVIDADE FÍSICA
################################### */
print("<h2>Motivações - Atividade física</h2>");
require $models."motivacao.class.php";

$motivacao = new Motivacao();

$motivacao->setDados(1, "Prevenção de doenças");
$motivacao->inserir($con);
$motivacao->setDados(2, "Indicação médica");
$motivacao->inserir($con);
$motivacao->setDados(3, "Estética");
$motivacao->inserir($con);
$motivacao->setDados(4, "Lazer");
$motivacao->inserir($con);
$motivacao->setDados(5, "Condicionamento físico");
$motivacao->inserir($con);
$motivacao->setDados(6, "Ganho de massa magra");
$motivacao->inserir($con);
$motivacao->setDados(7, "Perda de gordura");
$motivacao->inserir($con);

$motivacoes = new Motivacao();
$lista_motivacoes = $motivacoes->listar($con);
print("<ul>");
while($mot = $lista_motivacoes->fetch_assoc()){
	print("<li>".$mot["id_motivacao"].": ".$mot["desc_motivacao"]."</li>");
}
print("</ul>");

/* ##################################
	GRAUS DE IMPORTÂNCIA
################################### */
print("<h2>Grau de importância - Atividade física</h2>");
require $models."grau_importancia.class.php";

$grau = new Grau_importancia();

$grau->setDados(1, "Não sei");
$grau->inserir($con);
$grau->setDados(2, "Determinante");
$grau->inserir($con);
$grau->setDados(3, "Importante");
$grau->inserir($con);
$grau->setDados(4, "Pouco importante");
$grau->inserir($con);
$grau->setDados(5, "Irrelevante");
$grau->inserir($con);

$graus_importancia = new Grau_importancia();
$lista_graus_importancia = $graus_importancia->listar($con);
print("<ul>");
while($grau = $lista_graus_importancia->fetch_assoc()){
	print("<li>".$grau["id_grau_importancia"].": ".$grau["desc_grau_importancia"]."</li>");
}
print("</ul>");

/* ##################################
	DOBRAS CUTANEAS
################################### */
print("<h2>Dobras Cutâneas</h2>");
require $models."dobra_cutanea.class.php";

$dobra_cutanea = new Dobra_cutanea();

$dobra_cutanea->setDados(1, "Abdominal", "ab");
$dobra_cutanea->inserir($con);
$dobra_cutanea->setDados(2, "Axilar média", "am");
$dobra_cutanea->inserir($con);
$dobra_cutanea->setDados(3, "Coxa", "cx");
$dobra_cutanea->inserir($con);
$dobra_cutanea->setDados(4, "Peitoral", "pe");
$dobra_cutanea->inserir($con);
$dobra_cutanea->setDados(5, "Subescapular", "sb");
$dobra_cutanea->inserir($con);
$dobra_cutanea->setDados(6, "Suprailíaca", "si");
$dobra_cutanea->inserir($con);
$dobra_cutanea->setDados(7, "Tríceps", "tr");
$dobra_cutanea->inserir($con);

$lista_dobras_cutaneas = $dobra_cutanea->listar($con);
print("<ul>");
while($dobra = $lista_dobras_cutaneas->fetch_assoc()){
	print("<li>".$dobra["id_dobra_cutanea"].": ".$dobra["desc_dobra_cutanea"]." (".$dobra["sigla_dobra"].")</li>");
}
print("</ul>");