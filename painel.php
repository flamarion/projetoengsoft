<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Painel";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header; 
			require $menu;
		?>
		<section class="row"> <!-- content !-->
			<section class="ten wide column">
				<h3 class="ui header">
					<i class="user icon"></i>
					<section class="content">
						Olá, <?php echo $_SESSION["usuario"]["nome"] ?>!
					</section>
				</h3>

				<?php
					require $models."avaliacao.class.php";
					require $classes."mensagem.class.php";

					$verificacao = new Avaliacao();
					$avaliacoes_incompletas = $verificacao->getAvaliacoesIncompletas($con, $_SESSION["usuario"]["id"]);
					if($avaliacoes_incompletas["quantidade"] > 0){
						$mensagem = new Mensagem(2, "Aviso importante!");
						$mensagem->addMensagem("Existem ".$avaliacoes_incompletas["quantidade"]." avaliações que não foram concluídas!");
						$mensagem->getMensagem();
					}
				?>

				<section class="ui segment">
				<?php ?>
				</section>
			</section>
		</section>
		<?php
			require $footer;
		?>
</body>

</html>
