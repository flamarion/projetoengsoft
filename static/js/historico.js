$(document).ready(function(){
	$('.ui.checkbox').checkbox();
	questoes = ["consumo_alcool", "consumo_fumo", "problema_cardiovascular", "problema_cardiovascular_familiar", "problema_ortopedico", "consumo_medicamentos", "outras_doencas_problemas", "problema_menstrual"];

	for(var q in questoes)
	{
		questao = questoes[q];
		if($('#'+questao).val() == 1){
			$('input[type="checkbox"][value="'+questao+'"]').prop("checked", true);
		}
	}

	$('input[type="text"][name="pressao_arterial_maxima"]').val($('input[type="hidden"][id="pressao_arterial_maxima"]').val());
	$('input[type="text"][name="pressao_arterial_minima"]').val($('input[type="hidden"][id="pressao_arterial_minima"]').val());
	$('input[type="text"][name="frequencia_cardiaca"]').val($('input[type="hidden"][id="frequencia_cardiaca"]').val());
	$('textarea[name="observacoes"]').val($('input[type="hidden"][id="observacoes"]').val());
})