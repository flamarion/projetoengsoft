$(document).ready(function(){

	dobras_antropometricas = Array('sb','tr','pe','am','si','ab','cx');

	$('#antropometria input[type="text"]').change(function(){
		id = $(this).attr('id');
		calcular_media(id);
	})

	function calcular_media(id){
		/* Calcula média a cada atualização de input */
		id = id.split("_");
		dobra = id[0];			
		medidas = Array();

		medida_1 = $('#'+dobra+'_medida_1').val();
		if(medida_1 != '')
			medidas.push(medida_1);
		medida_2 = $('#'+dobra+'_medida_2').val();
		if(medida_2 != '')
			medidas.push(medida_2);
		medida_3 = $('#'+dobra+'_medida_3').val();
		if(medida_3 != '')
			medidas.push(medida_3);
		
		media = $('#'+dobra+'_medida_media');
		
		qtd_medidas = 0;
		soma = 0;
		for(var m in medidas){
			soma += parseFloat(medidas[m]);
			qtd_medidas += 1;
		}
		
		valor_media = soma/qtd_medidas;
		if(!isNaN(valor_media))
			media.val(valor_media);
		else
			media.val(0);
	}

	/*
	$('#select_protocolo_antropometrico').change(function selecionarProtocoloAntropometrico(){
		dobras_medidas = $(this).val();
		id = $('#select_protocolo_antropometrico option[value="'+dobras_medidas+'"]').attr('id');
		$('#input_protocolo_antropometrico').val(id);
		$('#calcular').prop("disabled", false);
		marcarDobrasRequiridas(dobras_medidas.split(";"));
	})
	*/

	/*
	function marcarDobrasRequiridas(dobras){
		for(var d in dobras_antropometricas){
			dobra = dobras_antropometricas[d];
			linha = $('#linha_'+dobra);
			linha.removeClass("dobra_utilizada");
			if(dobras.indexOf(dobra) > -1)
				linha.addClass("dobra_utilizada");
		}
	}
	*/

	/*
	if($('#registros').val())
		autoPreencher();


	function autoPreencher(){
		fields = [
			"altura", 
			"peso", 
			"densidade_corporal", 
			"massa_magra", 
			"massa_gorda", 
			"percentual_de_gordura", 
			"peso_maximo_recomendavel", 
			"objetivo_de_emagrecimento"
		];
		
		/* preenche (alguns) campos do formulário */
		// for(var f in fields){
		// 	field = fields[f];
		// 	$('input[name="'+field+'"]').val($('input[type="hidden"][id="'+field+'"]').val());
		// }

		/* preenche os valores das (médias) dobras */
		// for(var d in dobras_antropometricas){
		// 	dobra = dobras_antropometricas[d];
		// 	valor_atual = $('#medida_'+dobra).val();
		// 	$('#'+dobra+'_medida_media').val(valor_atual);
		// }

		/* "setar" select do protocolo */
		// id_protocolo_atual = $('#id_protocolo_antropometrico_id').val();
		// medidas_atuais = $('#select_protocolo_antropometrico option[id="'+id_protocolo_atual+'"]').val();
		// marcarDobrasRequiridas(medidas_atuais);
		// $('#select_protocolo_antropometrico').dropdown('set selected', medidas_atuais);
		
		/* "setar" select do condicionamento */
		// condicionamento = $('#condicionamento').val();
		// $('select[name="condicionamento"]').dropdown('set selected', condicionamento);

		/* observacoes */
		// observacoes = $('#observacoes').val();
		// $('textarea[name="observacoes"]').val(observacoes);
	//}
	//

	altura = $('input[name="altura"]');
	peso = $('input[name="peso"]');

	if($('#preenchimento').val() == 1){

		altura.val($('#valor_altura').val());
		peso.val($('#valor_peso').val());

		for(var d in dobras_antropometricas){
			dobra = dobras_antropometricas[d];
			valor_medida = $('#valor_'+dobra);
			$('input[name="'+dobra+'_medida"]').val(valor_medida.val());
		}
	}
})