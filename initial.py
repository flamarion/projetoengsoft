from graphics import *

numero_de_lados = int(input("Número da lados da figura: " ))
while numero_de_lados >= 3:
    janela = GraphWin()
    pontos = []
    print("Clique o Mouse!")
    for i in range(numero_de_lados):
        p = janela.getMouse()
        print("Ponto: ",p.getX(),p.getY())
        pontos.append((p.getX(),p.getY()))
    for i,ponto in enumerate(pontos):
        if i < len(pontos)-1:
            linha = Line(Point(ponto[0],ponto[1]), Point(pontos[i+1][0],pontos[i+1][1]))
        else:
            linha = Line(Point(ponto[0],ponto[1]), Point(pontos[0][0],pontos[0][1]))
        linha.draw(janela)
    print("...")
    break
