<?php

function sortCor(){
	$cores = array(
			"orange",
			"yellow",
			"purple",
			"red",
			"pink",
			"black",
			"blue",
			"teal",
			"green"
		);
	
	$cor = $cores[array_rand($cores)];
	return $cor;
}

function trocaCor($cor1, $cor2, $indice){
	if($indice % 2 == 0)
		return $cor1;
	else
		return $cor2;
}