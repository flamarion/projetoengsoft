<?php

function idade($data)
{
	if($data != null && $data != "")
	{
		$array_data = explode("-", $data);
		$dia = (int) $array_data[2];
		$mes = (int) $array_data[1];
		$ano = (int) $array_data[0];
		
		date_default_timezone_set('America/Sao_Paulo');
		$atual = getdate();

		$dia_atual = (int) $atual["mday"];
		$mes_atual = (int) $atual["mon"];
		$ano_atual = (int) $atual["year"];

		$idade = $ano_atual - $ano;
		if($mes_atual < $mes)
			return $idade - 1;
		else if($mes_atual == $mes)
			if($dia_atual < $dia)
				return $idade - 1;
			else
				return $idade;
		else
			return $idade;
	}
	return null;
}