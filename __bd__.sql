-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 12-Ago-2016 às 15:01
-- Versão do servidor: 5.5.49-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `ava_2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `amostra_saude`
--

CREATE TABLE IF NOT EXISTS `amostra_saude` (
  `id_amostra_saude` int(11) NOT NULL AUTO_INCREMENT,
  `id_avaliacao_id` int(11) NOT NULL,
  `desc_atividades_fisicas` varchar(200) DEFAULT NULL,
  `tempo_pratica_atividades` varchar(30) DEFAULT NULL,
  `frequencia_semanal_pratica` int(11) DEFAULT NULL,
  `frequencia_academia` int(11) DEFAULT NULL,
  `observacoes` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_amostra_saude`),
  KEY `fk_avaliacao_amostra_saude` (`id_avaliacao_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `amostra_saude`
--

INSERT INTO `amostra_saude` (`id_amostra_saude`, `id_avaliacao_id`, `desc_atividades_fisicas`, `tempo_pratica_atividades`, `frequencia_semanal_pratica`, `frequencia_academia`, `observacoes`) VALUES
(3, 12, 'Futebol, caminhada, corrida', '2 anos', 2, 5, 'Nada'),
(4, 8, 'Caminhada', '5 meses', 2, 4, 'Nada'),
(5, 13, 'Ã§aksllfkalkfalkÃ§faksÃ§l', '4 meses', 3, 4, 'KASOKPAKPOSKOASDLMSAMF');

-- --------------------------------------------------------

--
-- Estrutura da tabela `antropometria`
--

CREATE TABLE IF NOT EXISTS `antropometria` (
  `id_antropometria` int(11) NOT NULL AUTO_INCREMENT,
  `id_avaliacao_id` int(11) NOT NULL,
  `altura` int(11) DEFAULT NULL,
  `peso` float DEFAULT NULL,
  `massa_magra` float DEFAULT NULL,
  `massa_gorda` float DEFAULT NULL,
  `densidade_corporal` float DEFAULT NULL,
  `percentual_gordura` float DEFAULT NULL,
  `peso_max_recomendavel` float DEFAULT NULL,
  `objetivo_emagrecimento` float DEFAULT NULL,
  `observacoes` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_antropometria`),
  KEY `fk_avaliacao_antropometria` (`id_avaliacao_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `antropometria`
--

INSERT INTO `antropometria` (`id_antropometria`, `id_avaliacao_id`, `altura`, `peso`, `massa_magra`, `massa_gorda`, `densidade_corporal`, `percentual_gordura`, `peso_max_recomendavel`, `objetivo_emagrecimento`, `observacoes`) VALUES
(1, 12, 177, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 8, 173, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 13, 158, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

CREATE TABLE IF NOT EXISTS `avaliacao` (
  `id_avaliacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_status_avaliacao_id` int(11) NOT NULL,
  `id_vinculacao_id` int(11) NOT NULL,
  `data_hora` datetime DEFAULT NULL,
  PRIMARY KEY (`id_avaliacao`),
  KEY `fk_vinculacao_avaliacao` (`id_vinculacao_id`),
  KEY `fk_status_avaliacao` (`id_status_avaliacao_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `avaliacao`
--

INSERT INTO `avaliacao` (`id_avaliacao`, `id_status_avaliacao_id`, `id_vinculacao_id`, `data_hora`) VALUES
(8, 1, 13, NULL),
(9, 1, 6, NULL),
(10, 1, 6, NULL),
(11, 1, 1, NULL),
(12, 1, 16, NULL),
(13, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `id_pessoa_id` int(11) NOT NULL,
  PRIMARY KEY (`id_cliente`),
  KEY `fk_pessoa_cliente` (`id_pessoa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `id_pessoa_id`) VALUES
(3, 19),
(4, 20),
(5, 21),
(6, 22),
(7, 23),
(8, 24),
(9, 25),
(10, 26),
(11, 27),
(12, 28),
(13, 29),
(14, 30),
(15, 31),
(16, 33),
(17, 34),
(18, 35);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `email` varchar(64) DEFAULT NULL,
  `telefone` varchar(24) DEFAULT NULL,
  `celular` varchar(24) DEFAULT NULL,
  `id_pessoa_id` int(11) NOT NULL,
  KEY `fk_pessoa_contato` (`id_pessoa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`email`, `telefone`, `celular`, `id_pessoa_id`) VALUES
('marcela.leite@ifc-araquari.edu.br', '', '', 13),
('marilia.ribeiro@gmail.com', '', '(47)99938809', 19),
('nicole.oliveira@gmail.com', '', '(47)88501033', 20),
('nessabrolim@gmail.com', '', '(47)91101580', 21),
('patrick.reis@hotmail.com', '', '(47)99289920', 22),
('feforock_999@gmail.com', '', '', 23),
('iago.marinheiro@bol.com.br', '', '(47)96898010', 24),
('waesser@gmail.com', '', '(47)98911505', 25),
('ivo.riegel@ifc-araquari.edu.br', '', '', 26),
('rafaelzit0_@gmail.com', '(47)38919930', '(47)98761500', 27),
('thiago.locatelli@gmail.com', '(47)34489010', '', 28),
('lucas.emanoel@gmail.com', '', '(47)99313899', 29),
('wellwill1992@gmail.com', '', '', 30),
('bruno.morais@gmail.com', '', '(47)88159603', 31),
('alan.elias81@hotmail.com', '', '', 32),
('joelmir.lopes@ifc-araquari.edu.br', '', '(47)99178090', 33),
('rubens.sfs@gmail.com', '', '(47)84899020', 34),
('thiago.locatelli@gmail.com', '', '(47)99381018', 35);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dobra_cutanea`
--

CREATE TABLE IF NOT EXISTS `dobra_cutanea` (
  `id_dobra_cutanea` int(11) NOT NULL AUTO_INCREMENT,
  `desc_dobra_cutanea` varchar(20) DEFAULT NULL,
  `sigla_dobra` char(2) DEFAULT NULL,
  PRIMARY KEY (`id_dobra_cutanea`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `dobra_cutanea`
--

INSERT INTO `dobra_cutanea` (`id_dobra_cutanea`, `desc_dobra_cutanea`, `sigla_dobra`) VALUES
(1, 'Abdominal', 'ab'),
(2, 'Axilar mÃ©dia', 'am'),
(3, 'Coxa', 'cx'),
(4, 'Peitoral', 'pe'),
(5, 'Subescapular', 'sb'),
(6, 'SuprailÃ­aca', 'si'),
(7, 'TrÃ­ceps', 'tr');

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE IF NOT EXISTS `endereco` (
  `cep` varchar(16) DEFAULT NULL,
  `rua` varchar(64) DEFAULT NULL,
  `numero` varchar(5) DEFAULT NULL,
  `bairro` varchar(32) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `uf` char(2) DEFAULT NULL,
  `observacoes` varchar(120) DEFAULT NULL,
  `id_pessoa_id` int(11) NOT NULL,
  KEY `fk_endereco_uf` (`uf`),
  KEY `fk_pessoa_endereco` (`id_pessoa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`cep`, `rua`, `numero`, `bairro`, `cidade`, `uf`, `observacoes`, `id_pessoa_id`) VALUES
('88272000', 'Rua MaurÃ­lio Silva', '93', 'Itaum', 'Joinville', 'SC', '', 19),
('88272000', 'Rua BogotÃ¡', '1019', 'Floresta', 'Joinville', 'SC', '', 20),
('82772000', 'Rua Porto Alegre', '108', 'Ubatuba', 'SÃ£o Francisco do Sul', 'SC', '', 21),
('89201000', 'Rua Adriano Ricardo', '20', 'Boavista', 'Joinville', 'SC', '', 22),
('82350000', 'Avenida SÃ£o Francisco do Sul', '1717', 'Costeira', 'BalneÃ¡rio Barra do Sul', 'SC', '', 23),
('88272000', 'Rua Adriano Ricardo', '20', 'Boavista', 'Joinville', 'SC', '', 24),
('87310000', 'Avenida Paranaguamirim', '24', 'Itinga', 'Joinville', 'SC', '', 25),
('89373000', 'Rua PragmÃ¡tica', '9281', 'Costa e Silva', 'Joinville', 'SC', '', 26),
('89240000', 'Rua dos Veados Selvagens', '100', 'AcaraÃ­', 'SÃ£o Francisco do Sul', 'SC', '', 27),
('89230000', 'Rua Almirante Bittencourt', '230', 'Areias', 'BalneÃ¡rio Barra do Sul', 'SC', '', 28),
('89240000', 'Rua JosÃ© Miguel Lopes Filho', '2910', 'Rocio Pequeno', 'SÃ£o Francisco do Sul', 'SC', 'Ao lado da pousada felÃ­cio', 29),
('87645000', 'Rua Coronel Moreira Mendes', '876', 'Escolinha', 'Joinville', 'SC', '', 30),
('89240000', 'Rua GuaÃ­ra', '72', 'Ubatuba', 'SÃ£o Francisco do Sul', 'SC', '', 31),
('', '', '', 'Floresta', 'Joinville', 'SC', '', 33),
('8924000', 'Rua MoisÃ©s EmitÃ©rio dos Santos', '325', 'Paulas', 'SÃ£o Francisco do Sul', 'SC', '', 34),
('89350000', 'Rua Jonas do Amaral', '333', 'Areias', 'BalneÃ¡rio Barra do Sul', 'SC', '', 35);

-- --------------------------------------------------------

--
-- Estrutura da tabela `estado_civil`
--

CREATE TABLE IF NOT EXISTS `estado_civil` (
  `id_estado_civil` int(11) NOT NULL,
  `desc_estado_civil` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_estado_civil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `estado_civil`
--

INSERT INTO `estado_civil` (`id_estado_civil`, `desc_estado_civil`) VALUES
(1, 'Solteiro'),
(2, 'Casado'),
(3, 'Divorciado'),
(4, 'ViÃºvo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `etnia`
--

CREATE TABLE IF NOT EXISTS `etnia` (
  `id_etnia` int(11) NOT NULL,
  `desc_etnia` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_etnia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `etnia`
--

INSERT INTO `etnia` (`id_etnia`, `desc_etnia`) VALUES
(1, 'Pardo'),
(2, 'Negro'),
(3, 'Branco'),
(4, 'Oriental'),
(5, 'IndÃ­gena'),
(6, 'HispÃ¢nico');

-- --------------------------------------------------------

--
-- Estrutura da tabela `grau_importancia`
--

CREATE TABLE IF NOT EXISTS `grau_importancia` (
  `id_grau_importancia` int(11) NOT NULL,
  `desc_grau_importancia` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_grau_importancia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `grau_importancia`
--

INSERT INTO `grau_importancia` (`id_grau_importancia`, `desc_grau_importancia`) VALUES
(1, 'NÃ£o sei'),
(2, 'Determinante'),
(3, 'Importante'),
(4, 'Pouco importante'),
(5, 'Irrelevante');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico_saude`
--

CREATE TABLE IF NOT EXISTS `historico_saude` (
  `id_historico_saude` int(11) NOT NULL AUTO_INCREMENT,
  `id_avaliacao_id` int(11) NOT NULL,
  `consumo_alcool` int(11) DEFAULT NULL,
  `consumo_fumo` int(11) DEFAULT NULL,
  `consumo_medicamento` int(11) DEFAULT NULL,
  `problema_cardio` int(11) DEFAULT NULL,
  `problema_cardio_familiar` int(11) DEFAULT NULL,
  `problema_menstrual` int(11) DEFAULT NULL,
  `lesao` int(11) DEFAULT NULL,
  `observacoes` varchar(300) DEFAULT NULL,
  `pressao_arterial_min` float DEFAULT NULL,
  `pressao_arterial_max` float DEFAULT NULL,
  `frequencia_cardiaca` float DEFAULT NULL,
  PRIMARY KEY (`id_historico_saude`),
  KEY `fk_avaliacao_historico_saude` (`id_avaliacao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `medida_dobra`
--

CREATE TABLE IF NOT EXISTS `medida_dobra` (
  `id_antropometria_id` int(11) NOT NULL,
  `id_dobra_cutanea_id` int(11) NOT NULL,
  `valor_medida` float DEFAULT NULL,
  PRIMARY KEY (`id_antropometria_id`,`id_dobra_cutanea_id`),
  KEY `fk_dobra_cutanea_medida_dobra` (`id_dobra_cutanea_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `motivacao`
--

CREATE TABLE IF NOT EXISTS `motivacao` (
  `id_motivacao` int(11) NOT NULL,
  `desc_motivacao` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_motivacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `motivacao`
--

INSERT INTO `motivacao` (`id_motivacao`, `desc_motivacao`) VALUES
(1, 'PrevenÃ§Ã£o de doenÃ§as'),
(2, 'IndicaÃ§Ã£o mÃ©dica'),
(3, 'EstÃ©tica'),
(4, 'Lazer'),
(5, 'Condicionamento fÃ­sico'),
(6, 'Ganho de massa magra'),
(7, 'Perda de gordura');

-- --------------------------------------------------------

--
-- Estrutura da tabela `motivacao_atividade_fisica`
--

CREATE TABLE IF NOT EXISTS `motivacao_atividade_fisica` (
  `id_avaliacao_id` int(11) NOT NULL,
  `id_motivacao_id` int(11) NOT NULL,
  `id_grau_importancia_id` int(11) NOT NULL,
  PRIMARY KEY (`id_avaliacao_id`,`id_motivacao_id`),
  KEY `fk_motivacao_atividade_fisica_motivacao` (`id_motivacao_id`),
  KEY `fk_motivacao_atividade_fisica_grau_importancia` (`id_grau_importancia_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `motivacao_atividade_fisica`
--

INSERT INTO `motivacao_atividade_fisica` (`id_avaliacao_id`, `id_motivacao_id`, `id_grau_importancia_id`) VALUES
(8, 3, 1),
(8, 4, 1),
(12, 2, 1),
(12, 3, 1),
(12, 4, 1),
(13, 2, 1),
(13, 3, 1),
(13, 5, 1),
(8, 7, 2),
(12, 5, 2),
(13, 1, 2),
(13, 6, 2),
(8, 2, 3),
(12, 1, 3),
(12, 6, 3),
(12, 7, 3),
(13, 7, 3),
(8, 1, 4),
(8, 6, 4),
(13, 4, 4),
(8, 5, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perimetria`
--

CREATE TABLE IF NOT EXISTS `perimetria` (
  `id_perimetria` int(11) NOT NULL AUTO_INCREMENT,
  `id_avaliacao_id` int(11) NOT NULL,
  `pescoco` float DEFAULT NULL,
  `ombro` float DEFAULT NULL,
  `torax` float DEFAULT NULL,
  `abdomen` float DEFAULT NULL,
  `cintura` float DEFAULT NULL,
  `quadril` float DEFAULT NULL,
  `braco_dir` float DEFAULT NULL,
  `braco_esq` float DEFAULT NULL,
  `antebraco_dir` float DEFAULT NULL,
  `antebraco_esq` float DEFAULT NULL,
  `coxa_dir` float DEFAULT NULL,
  `coxa_esq` float DEFAULT NULL,
  `panturrilha_dir` float DEFAULT NULL,
  `panturrilha_esq` float DEFAULT NULL,
  PRIMARY KEY (`id_perimetria`),
  KEY `fk_avaliacao_perimetria` (`id_avaliacao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE IF NOT EXISTS `pessoa` (
  `id_pessoa` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(32) NOT NULL,
  `sobrenome` varchar(32) DEFAULT NULL,
  `data_nascimento` date NOT NULL,
  `cpf` varchar(20) DEFAULT NULL,
  `rg` varchar(20) DEFAULT NULL,
  `id_etnia_id` int(11) NOT NULL,
  `id_estado_civil_id` int(11) NOT NULL,
  `id_sexo_id` int(11) NOT NULL,
  PRIMARY KEY (`id_pessoa`),
  KEY `fk_etnia_pessoa` (`id_etnia_id`),
  KEY `fk_estado_civil_pessoa` (`id_estado_civil_id`),
  KEY `fk_sexo_pessoa` (`id_sexo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id_pessoa`, `nome`, `sobrenome`, `data_nascimento`, `cpf`, `rg`, `id_etnia_id`, `id_estado_civil_id`, `id_sexo_id`) VALUES
(10, 'Alan Elias', 'da ConceiÃ§Ã£o', '1981-06-21', '08978087531', '6798987', 3, 1, 1),
(11, 'Felipe', 'Flamarion', '1995-08-02', '09448940971', '6606433', 1, 1, 1),
(13, 'Marcela', 'Leite', '1980-04-25', '06375068711', '', 3, 2, 2),
(19, 'MarÃ­lia', 'Ribeiro', '1994-02-13', '08978087531', '', 1, 1, 2),
(20, 'Nicole', 'Oliveira', '1995-08-29', '07846542296', '', 3, 2, 2),
(21, 'Vanessa', 'Rolim', '1990-01-22', '', '', 3, 2, 2),
(22, 'Patrick Filipe', 'Reis', '1996-01-15', '', '', 2, 1, 1),
(23, 'Fernando Henrique', 'Okubo', '1988-11-19', '09163370510', '', 4, 1, 1),
(24, 'Iago', 'Marinheiro', '1992-04-25', '', '', 1, 1, 1),
(25, 'Wagner', 'Esser', '1995-03-24', '', '', 3, 1, 1),
(26, 'Ivo', 'Riegel', '1982-12-17', '', '', 3, 2, 1),
(27, 'Rafael JosÃ©', 'de Miranda Sobrinho', '1996-02-17', '07879540677', '9879855', 3, 1, 1),
(28, 'Thiago', 'Locatelli de Oliveira', '1997-11-08', '09478636675', '', 3, 1, 1),
(29, 'Lucas Emanoel', 'Fonseca', '1993-11-22', '08711592573', '', 1, 1, 1),
(30, 'Wellington William', 'Santos', '1992-02-06', '', '', 3, 1, 1),
(31, 'Bruno Miguel', 'Morais', '1998-07-21', '09785567575', '', 3, 1, 1),
(32, 'Alan Elias', 'ConceiÃ§Ã£o', '1981-06-21', '09754675511', '', 3, 1, 1),
(33, 'Joelmir JosÃ©', 'Lopes', '1986-04-22', '', '', 3, 2, 1),
(34, 'Rubens', 'Cidral', '1996-08-12', '', '', 1, 1, 1),
(35, 'Thiago', 'Locatelli de Oliveira', '1997-10-01', '087.645.133-67', '', 3, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sexo`
--

CREATE TABLE IF NOT EXISTS `sexo` (
  `id_sexo` int(11) NOT NULL,
  `desc_sexo` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_sexo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sexo`
--

INSERT INTO `sexo` (`id_sexo`, `desc_sexo`) VALUES
(1, 'Masculino'),
(2, 'Feminino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `status_avaliacao`
--

CREATE TABLE IF NOT EXISTS `status_avaliacao` (
  `id_status_avaliacao` int(11) NOT NULL,
  `desc_status_avaliacao` varchar(20) NOT NULL,
  PRIMARY KEY (`id_status_avaliacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `status_avaliacao`
--

INSERT INTO `status_avaliacao` (`id_status_avaliacao`, `desc_status_avaliacao`) VALUES
(1, 'aberta'),
(2, 'fechada');

-- --------------------------------------------------------

--
-- Estrutura da tabela `teste_fisico`
--

CREATE TABLE IF NOT EXISTS `teste_fisico` (
  `id_teste_fisico` int(11) NOT NULL AUTO_INCREMENT,
  `id_avaliacao_id` int(11) NOT NULL,
  PRIMARY KEY (`id_teste_fisico`),
  KEY `fk_teste_fisico_avaliacao` (`id_avaliacao_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `teste_flexibilidade`
--

CREATE TABLE IF NOT EXISTS `teste_flexibilidade` (
  `id_teste_fisico_id` int(11) NOT NULL,
  `sentar_alcancar` float DEFAULT NULL,
  `sentar_alcancar_wells` float DEFAULT NULL,
  KEY `fk_teste_fisico_flexibilidade` (`id_teste_fisico_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `teste_forca`
--

CREATE TABLE IF NOT EXISTS `teste_forca` (
  `id_teste_fisico_id` int(11) NOT NULL,
  `dinamometro_dorsal` float DEFAULT NULL,
  `dinamometro_escapular` float DEFAULT NULL,
  `preensao_manual` float DEFAULT NULL,
  KEY `fk_teste_fisico_forca` (`id_teste_fisico_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `teste_vo2`
--

CREATE TABLE IF NOT EXISTS `teste_vo2` (
  `id_teste_fisico_id` int(11) NOT NULL,
  `completado` int(11) DEFAULT NULL,
  `frequencia_cardiaca_final` float DEFAULT NULL,
  KEY `fk_teste_fisico_vo2` (`id_teste_fisico_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uf`
--

CREATE TABLE IF NOT EXISTS `uf` (
  `sigla_uf` char(2) NOT NULL,
  `desc_uf` varchar(40) NOT NULL,
  PRIMARY KEY (`sigla_uf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `uf`
--

INSERT INTO `uf` (`sigla_uf`, `desc_uf`) VALUES
('PR', 'ParanÃ¡'),
('RS', 'Rio Grande do Sul'),
('SC', 'Santa Catarina'),
('SP', 'SÃ£o Paulo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_pessoa_id` int(11) NOT NULL,
  `login` varchar(20) DEFAULT NULL,
  `senha` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_pessoa_usuario` (`id_pessoa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_pessoa_id`, `login`, `senha`) VALUES
(6, 10, 'alanelias', '$2y$10$6qzpdE6XraWRgCplLAMf4uYjTOhuxzrInEour7QQ7qjTs4XbA61UK'),
(7, 11, 'felipe', '$2y$10$/J6gwoPVcW9L3I1IqtGgFuS65.l295SE4wMhkp9gydi1H8f4TnSP2'),
(8, 13, 'marcela', '$2y$10$ujwGIYOzEdkO1we5EQG2qu.rI6EdoNAdz3fDYamILPhsb3W/W8fmK'),
(9, 32, 'alan', '$2y$10$1nw3CaGlKuj8PkgUS9jMW.sZQZgcrsPXR4eiJGIdrXlb6kQ52IS7K');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vinculacao`
--

CREATE TABLE IF NOT EXISTS `vinculacao` (
  `id_vinculacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente_id` int(11) NOT NULL,
  `id_usuario_id` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id_vinculacao`),
  KEY `fk_cliente_vinculacao` (`id_cliente_id`),
  KEY `fk_usuario_vinculacao` (`id_usuario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `vinculacao`
--

INSERT INTO `vinculacao` (`id_vinculacao`, `id_cliente_id`, `id_usuario_id`, `data`) VALUES
(1, 3, 8, NULL),
(2, 4, 8, NULL),
(3, 5, 8, NULL),
(4, 6, 8, NULL),
(5, 7, 8, NULL),
(6, 8, 8, NULL),
(7, 9, 8, NULL),
(8, 10, 8, NULL),
(9, 11, 8, NULL),
(10, 12, 8, NULL),
(11, 13, 8, NULL),
(12, 14, 8, NULL),
(13, 15, 8, NULL),
(14, 16, 9, NULL),
(15, 17, 9, NULL),
(16, 18, 6, NULL);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `amostra_saude`
--
ALTER TABLE `amostra_saude`
  ADD CONSTRAINT `fk_avaliacao_amostra_saude` FOREIGN KEY (`id_avaliacao_id`) REFERENCES `avaliacao` (`id_avaliacao`);

--
-- Limitadores para a tabela `antropometria`
--
ALTER TABLE `antropometria`
  ADD CONSTRAINT `fk_avaliacao_antropometria` FOREIGN KEY (`id_avaliacao_id`) REFERENCES `avaliacao` (`id_avaliacao`);

--
-- Limitadores para a tabela `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD CONSTRAINT `fk_status_avaliacao` FOREIGN KEY (`id_status_avaliacao_id`) REFERENCES `status_avaliacao` (`id_status_avaliacao`),
  ADD CONSTRAINT `fk_vinculacao_avaliacao` FOREIGN KEY (`id_vinculacao_id`) REFERENCES `vinculacao` (`id_vinculacao`);

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_pessoa_cliente` FOREIGN KEY (`id_pessoa_id`) REFERENCES `pessoa` (`id_pessoa`);

--
-- Limitadores para a tabela `contato`
--
ALTER TABLE `contato`
  ADD CONSTRAINT `fk_pessoa_contato` FOREIGN KEY (`id_pessoa_id`) REFERENCES `pessoa` (`id_pessoa`);

--
-- Limitadores para a tabela `endereco`
--
ALTER TABLE `endereco`
  ADD CONSTRAINT `fk_endereco_uf` FOREIGN KEY (`uf`) REFERENCES `uf` (`sigla_uf`),
  ADD CONSTRAINT `fk_pessoa_endereco` FOREIGN KEY (`id_pessoa_id`) REFERENCES `pessoa` (`id_pessoa`);

--
-- Limitadores para a tabela `historico_saude`
--
ALTER TABLE `historico_saude`
  ADD CONSTRAINT `fk_avaliacao_historico_saude` FOREIGN KEY (`id_avaliacao_id`) REFERENCES `avaliacao` (`id_avaliacao`);

--
-- Limitadores para a tabela `medida_dobra`
--
ALTER TABLE `medida_dobra`
  ADD CONSTRAINT `fk_antropometria_medida_dobra` FOREIGN KEY (`id_antropometria_id`) REFERENCES `antropometria` (`id_antropometria`),
  ADD CONSTRAINT `fk_dobra_cutanea_medida_dobra` FOREIGN KEY (`id_dobra_cutanea_id`) REFERENCES `dobra_cutanea` (`id_dobra_cutanea`);

--
-- Limitadores para a tabela `motivacao_atividade_fisica`
--
ALTER TABLE `motivacao_atividade_fisica`
  ADD CONSTRAINT `fk_motivacao_atividade_fisica_avaliacao` FOREIGN KEY (`id_avaliacao_id`) REFERENCES `avaliacao` (`id_avaliacao`),
  ADD CONSTRAINT `fk_motivacao_atividade_fisica_grau_importancia` FOREIGN KEY (`id_grau_importancia_id`) REFERENCES `grau_importancia` (`id_grau_importancia`),
  ADD CONSTRAINT `fk_motivacao_atividade_fisica_motivacao` FOREIGN KEY (`id_motivacao_id`) REFERENCES `motivacao` (`id_motivacao`);

--
-- Limitadores para a tabela `perimetria`
--
ALTER TABLE `perimetria`
  ADD CONSTRAINT `fk_avaliacao_perimetria` FOREIGN KEY (`id_avaliacao_id`) REFERENCES `avaliacao` (`id_avaliacao`);

--
-- Limitadores para a tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD CONSTRAINT `fk_estado_civil_pessoa` FOREIGN KEY (`id_estado_civil_id`) REFERENCES `estado_civil` (`id_estado_civil`),
  ADD CONSTRAINT `fk_etnia_pessoa` FOREIGN KEY (`id_etnia_id`) REFERENCES `etnia` (`id_etnia`),
  ADD CONSTRAINT `fk_sexo_pessoa` FOREIGN KEY (`id_sexo_id`) REFERENCES `sexo` (`id_sexo`);

--
-- Limitadores para a tabela `teste_fisico`
--
ALTER TABLE `teste_fisico`
  ADD CONSTRAINT `fk_teste_fisico_avaliacao` FOREIGN KEY (`id_avaliacao_id`) REFERENCES `avaliacao` (`id_avaliacao`);

--
-- Limitadores para a tabela `teste_flexibilidade`
--
ALTER TABLE `teste_flexibilidade`
  ADD CONSTRAINT `fk_teste_fisico_flexibilidade` FOREIGN KEY (`id_teste_fisico_id`) REFERENCES `teste_fisico` (`id_teste_fisico`);

--
-- Limitadores para a tabela `teste_forca`
--
ALTER TABLE `teste_forca`
  ADD CONSTRAINT `fk_teste_fisico_forca` FOREIGN KEY (`id_teste_fisico_id`) REFERENCES `teste_fisico` (`id_teste_fisico`);

--
-- Limitadores para a tabela `teste_vo2`
--
ALTER TABLE `teste_vo2`
  ADD CONSTRAINT `fk_teste_fisico_vo2` FOREIGN KEY (`id_teste_fisico_id`) REFERENCES `teste_fisico` (`id_teste_fisico`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_pessoa_usuario` FOREIGN KEY (`id_pessoa_id`) REFERENCES `pessoa` (`id_pessoa`);

--
-- Limitadores para a tabela `vinculacao`
--
ALTER TABLE `vinculacao`
  ADD CONSTRAINT `fk_cliente_vinculacao` FOREIGN KEY (`id_cliente_id`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `fk_usuario_vinculacao` FOREIGN KEY (`id_usuario_id`) REFERENCES `usuario` (`id_usuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
