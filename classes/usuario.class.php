<?php

require "pessoa.class.php";

class Usuario{
	
	private $dados;
	private $id_pessoa;

	function __construct($dados){
		$this->dados = $dados;
	}

	function inserirPessoa($con){
		try{
			$pessoa = new Pessoa();
			$pessoa->setDados($this->dados);
			$this->id_pessoa = $pessoa->saveDados($con);
		}catch(Exception $e){
			$this->id_pessoa = null;
		}
	}

	function inserirUsuario($con){
		$login = $this->dados["login"];
		$senha = $this->criptSenha($this->dados["senha"]);
		
		$valores = " VALUES('$login', '$senha', $this->id_pessoa)";
		try{
			mysqli_query($con, "INSERT INTO usuario(login, senha, id_pessoa_id) ".$valores);
			return true;
		}catch(Exception $e){
			return false;
		}
	}

	function criptSenha($senha){
		return password_hash($senha, PASSWORD_BCRYPT);
	}
	

}