<?php

class LogIn{

	private $login;
	private $senha;
	private $dados;
	private $erro;

	function __construct($login, $senha){
		$this->login = $login;
		$this->senha = $senha;
	}

	function setLogin($login){
		$this->login = $login;
	}

	function setSenha($senha){
		$this->senha = $senha;
	}

	function autenticar($con){
		//$query = "SELECT u.id_usuario, p.nome, u.login, u.senha FROM usuario u, pessoa p WHERE u.login = '$this->login' AND p.id_pessoa = u.id_pessoa_id";
		$sql = "SELECT usuario.*, pessoa.nome FROM usuario INNER JOIN pessoa ON (id_pessoa = id_pessoa_id) WHERE login = '$this->login'";

		if($busca = $con->query($sql)){
			if($busca->num_rows == 1){
				$dados = $busca->fetch_assoc();
				if(password_verify($this->senha, $dados["senha"])){
					$dados_usuario = array(
							"id" => $dados["id_usuario"],
							"pessoa" => $dados["id_pessoa_id"],
							"nome" => $dados["nome"]
						);
					$this->dados = $dados_usuario;
					return true;
				}
				else
					$this->erro = 'Senha incorreta!';
			}
			else
				$this->erro = 'Usuário "'.$this->login.'" não está cadastrado!';
		}
		else
			$this->erro = $con->error;
		return false;
	}

	function getDadosUsuario(){
		return $this->dados;
	}

	function getErro(){
		return $this->erro;
	}

}