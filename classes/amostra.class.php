<?php

//require "classes/etapa.class.php";

class AmostraDeSaude{

	// private $avaliacao;
	private $avaliacao;
	private $dados;
	public $NOME_TABELA = "amostra_de_saude";

	public function __construct($avaliacao){
		//parent::__construct($id_avaliacao);
		$this->avaliacao = $avaliacao;
	}

	function setDados($dados){
		$this->dados = $dados;
	}

	function inserir($con){
		if($this->dados != null){
			try{
				$pratica_atividade_fisica = $this->dados["pratica_atividade_fisica"];
				$descricao_atividades = $this->dados["descricao_atividades"];
				$tempo_pratica_atividade_fisica = $this->dados["tempo_pratica_atividade_fisica"]." ".$this->dados["contagem_de_tempo"];
				$frequencia_atividade_fisica = $this->dados["frequencia_atividade_fisica"];
				$frequencia_academia = $this->dados["frequencia_academia"];
				$observacoes = $this->dados["observacoes"];

				$script = "INSERT INTO $this->NOME_TABELA VALUES($this->avaliacao, $pratica_atividade_fisica, '$descricao_atividades', '$tempo_pratica_atividade_fisica', $frequencia_atividade_fisica, $frequencia_academia, '$observacoes')";
				//print($script);
				
				$con->query($script);
				return true;
			}
			catch(Exception $e){
				return false;
			}
		}
		return false;
	}

	function buscar_registros($con){
		try{
			$consulta = $con->query("SELECT * FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows == 1)
				return $consulta;
			return null;
		}
		catch(Exception $e){
			print("Não foi possível buscar os registros!");
		}
	}

	function estaFeito($con){
		# Verifica se para esta avaliação, já houve um registro do tipo AVALIAÇÃO - MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT id_avaliacao_id FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows > 0)
				return true;
			else
				return false;
		}
		catch(Exception $e){
			print("Não foi possível verificar registros!");
		}
	}

	function atualizar($con){
		if($this->dados != null){
			try{
				$pratica_atividade_fisica = $this->dados["pratica_atividade_fisica"];
				$descricao_atividades = $this->dados["descricao_atividades"];
				$tempo_pratica_atividade_fisica = $this->dados["tempo_pratica_atividade_fisica"]." ".$this->dados["contagem_de_tempo"];
				$frequencia_atividade_fisica = $this->dados["frequencia_atividade_fisica"];
				$frequencia_academia = $this->dados["frequencia_academia"];
				$observacoes = $this->dados["observacoes"];

				$script = "UPDATE $this->NOME_TABELA SET pratica_atividade_fisica = $pratica_atividade_fisica,
				descricao_atividades = '$descricao_atividades',
				tempo_pratica_atividade_fisica = '$tempo_pratica_atividade_fisica',
				frequencia_atividade_fisica = $frequencia_atividade_fisica,
				frequencia_academia = $frequencia_academia,
				observacoes = '$observacoes'
				WHERE id_avaliacao_id = $this->avaliacao
				";
				
				$con->query($script);
				return true;
			}
			catch(Exception $e){
				return false;
			}
		}
		return false;
	}
}