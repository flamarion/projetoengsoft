<?php

class Endereco{

	private $NOME_TABELA = "endereco";

	private $dados;
	private $id_endereco;
	private $id_pessoa;

	private $PESSOA = "id_pessoa_id";
	private $CEP = "cep";
	private $RUA = "rua";
	private $NUM = "numero";
	private $BAIRRO = "bairro";
	private $CIDADE = "cidade";
	private $UF = "sigla_estado";

	function __construct(){}

	function setIdPessoa($id_pessoa){
		$this->id_pessoa = $id_pessoa;
	}

	function setIdEndereco($id_endereco){
		$this->id_endereco = $id_endereco;
	}

	function setDados($dados){
		$this->dados = $dados;
	}

	function getIdEndereco(){
		return $this->id_endereco;
	}

	function getScriptInsert(){
		if($this->dados != null && $this->id_pessoa != null){
			$colunas = array();
			$valores = array();

			array_push($colunas, $this->PESSOA);
			array_push($valores, $this->id_pessoa);
			array_push($colunas, $this->CEP);
			array_push($valores, "'".$this->dados[$this->CEP]."'");
			array_push($colunas, $this->RUA);
			array_push($valores, "'".$this->dados[$this->RUA]."'");
			array_push($colunas, $this->NUM);
			array_push($valores, "'".$this->dados[$this->NUM]."'");
			array_push($colunas, $this->BAIRRO);
			array_push($valores, "'".$this->dados[$this->BAIRRO]."'");
			array_push($colunas, $this->CIDADE);
			array_push($valores, "'".$this->dados[$this->CIDADE]."'");
			array_push($colunas, $this->UF);
			array_push($valores, "'".$this->dados[$this->UF]."'");

			$colunas = implode(", ", $colunas);
			$valores = implode(", ", $valores);

			$query = "INSERT INTO $this->NOME_TABELA ($colunas) VALUES ($valores)";
			return $query;
		}
		print("<Endereço> Não foi possível formular o script!");
		return null;
	}

	function inserir($con){
		try{
			$query = $this->getScriptInsert();
			if($query != null){
				$con->query($query);
				return true;
			}
		}
		catch(Exception $e){}
		return false;
	}

	function busca($con){
		try{
			$busca = $con->query("SELECT * FROM $this->NOME_TABELA WHERE id_pessoa_id = $this->id_pessoa");
			if($busca->num_rows == 1){
				$endereco = $busca->fetch_assoc();
				return $endereco;
			}
		}
		catch(Exception $e)
		{}
		return null;
	}

}