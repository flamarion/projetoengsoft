<?php

class ClienteUsuario{

	private $NOME_TABELA = "cliente_usuario";

	private $CLIENTE = "id_cliente_id";
	private $USUARIO = "id_usuario_id";
	private $DATA = "data_inicio";
	private $STATUS = "id_status_id";

	private $id_cliente;
	private $id_usuario;

	function __construct($id_cliente, $id_usuario)
	{
		$this->id_cliente = $id_cliente;
		$this->id_usuario = $id_usuario;
	}

	function salvar($con)
	{	
		$status_default = 1;
		$query = "INSERT INTO $this->NOME_TABELA ($this->CLIENTE, $this->USUARIO, $this->STATUS) VALUES ($this->id_cliente, $this->id_usuario, $status_default)";
		try
		{
			$con->query($query);
			return true;
		}
		catch(Exception $e){}
		return false;
	}

	function isVinculado($con)
	{
		$query = "SELECT count(*) as contador FROM $this->NOME_TABELA WHERE id_cliente_id = $this->id_cliente AND id_usuario_id = $this->id_usuario";
		try{
			$busca = $con->query($query);
			$resultado = $busca->fetch_assoc();
			if($resultado["contador"] == 1)
				return true;
		}
		catch(Exception $e){}
		return false;
	}

}