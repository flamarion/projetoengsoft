<?php

//require "classes/etapa.class.php";

//class HistoricoDeSaude extends EtapaAvaliacao{

class HistoricoDeSaude{
	public $NOME_TABELA = "historico_de_saude";
	private $avaliacao;
	private $dados;
	private $questoes_historico = array(
				"consumo_alcool" => null,
				"consumo_fumo" => null,
				"problema_cardiovascular" => null,
				"problema_cardiovascular_familiar" => null,
				"problema_ortopedico" => null,
				"consumo_medicamentos" => null,
				"problema_menstrual" => null
			);

	private $CONSUMO_ALCOOL = "consumo_alcool";
	private $CONSUMO_FUMO = "consumo_fumo";
	private $CONSUMO_MEDICAMENTOS = "consumo_medicamentos";
	private $PROBLEMA_CARDIOVASCULAR = "problema_cardiovascular";
	private $PROBLEMA_CARDIOVASCULAR_FAMILIAR = "problema_cardiovascular_familiar";
	private $PROBLEMA_ORTOPEDICO = "problema_ortopedico";
	private $PROBLEMA_MENSTRUAL = "problema_menstrual";
	//private $OUTRAS_DOENCAS_PROBLEMAS = "outras_doencas_problemas";
	private $PRESSAO_ARTERIAL_MAXIMA = "pressao_arterial_maxima";
	private $PRESSAO_ARTERIAL_MINIMA = "pressao_arterial_minima";
	private $FREQUENCIA_CARDIACA = "frequencia_cardiaca";
	private $OBSERVACOES = "observacoes";

	public function __construct($avaliacao){
		$this->avaliacao = $avaliacao;
	}

	public function setDados($dados){
		$this->dados = $dados;
	}

	private function getResposta($field){
		if($this->dados != null){
			if(in_array($field, $this->dados["historico"]))
				return 1;
		}
		return 0;
	}

	private function getScriptUpdate(){
		if($this->dados != null)
		{
			$atualizacoes = array();

			array_push($atualizacoes, $this->CONSUMO_ALCOOL." = ".$this->getResposta($this->CONSUMO_ALCOOL));
			array_push($atualizacoes, $this->CONSUMO_FUMO." = ".$this->getResposta($this->CONSUMO_FUMO));
			array_push($atualizacoes, $this->CONSUMO_MEDICAMENTOS." = ".$this->getResposta($this->CONSUMO_MEDICAMENTOS));
			array_push($atualizacoes, $this->PROBLEMA_CARDIOVASCULAR." = ".$this->getResposta($this->PROBLEMA_CARDIOVASCULAR));
			array_push($atualizacoes, $this->PROBLEMA_CARDIOVASCULAR_FAMILIAR." = ".$this->getResposta($this->PROBLEMA_CARDIOVASCULAR_FAMILIAR));
			array_push($atualizacoes, $this->PROBLEMA_ORTOPEDICO." = ".$this->getResposta($this->PROBLEMA_ORTOPEDICO));
			array_push($atualizacoes, $this->PROBLEMA_MENSTRUAL." = ".$this->getResposta($this->PROBLEMA_MENSTRUAL));
			array_push($atualizacoes, $this->PRESSAO_ARTERIAL_MAXIMA." = ".$this->dados[$this->PRESSAO_ARTERIAL_MAXIMA]);
			array_push($atualizacoes, $this->PRESSAO_ARTERIAL_MINIMA." = ".$this->dados[$this->PRESSAO_ARTERIAL_MINIMA]);
			array_push($atualizacoes, $this->FREQUENCIA_CARDIACA." = ".$this->dados[$this->FREQUENCIA_CARDIACA]);
			array_push($atualizacoes, $this->OBSERVACOES." = '".$this->dados[$this->OBSERVACOES]."'");

			$atualizacoes = implode(",", $atualizacoes);
			$script = "UPDATE $this->NOME_TABELA SET $atualizacoes WHERE id_avaliacao_id = $this->avaliacao";
			return $script;
		}
		return null;
	}

	private function getScriptInsert(){
		if($this->dados != null)
		{
			$colunas = array();
			$valores = array();

			#AVALIAÇÃO REFERENTE
			array_push($colunas, "id_avaliacao_id");
			array_push($valores, $this->avaliacao);
			#QUESTÕES
			array_push($colunas, $this->CONSUMO_ALCOOL);
			array_push($valores, $this->getResposta($this->CONSUMO_ALCOOL));
			array_push($colunas, $this->CONSUMO_FUMO);
			array_push($valores, $this->getResposta($this->CONSUMO_FUMO));
			array_push($colunas, $this->CONSUMO_MEDICAMENTOS);
			array_push($valores, $this->getResposta($this->CONSUMO_MEDICAMENTOS));
			array_push($colunas, $this->PROBLEMA_CARDIOVASCULAR);
			array_push($valores, $this->getResposta($this->PROBLEMA_CARDIOVASCULAR));
			array_push($colunas, $this->PROBLEMA_CARDIOVASCULAR_FAMILIAR);
			array_push($valores, $this->getResposta($this->PROBLEMA_CARDIOVASCULAR_FAMILIAR));
			array_push($colunas, $this->PROBLEMA_ORTOPEDICO);
			array_push($valores, $this->getResposta($this->PROBLEMA_ORTOPEDICO));
			array_push($colunas, $this->PROBLEMA_MENSTRUAL);
			array_push($valores, $this->getResposta($this->PROBLEMA_MENSTRUAL));
			#PRESSÃO ARTERIAL - MÁXIMA
			array_push($colunas, $this->PRESSAO_ARTERIAL_MAXIMA);
			array_push($valores, $this->dados[$this->PRESSAO_ARTERIAL_MAXIMA]);
			#MINIMA
			array_push($colunas, $this->PRESSAO_ARTERIAL_MINIMA);
			array_push($valores, $this->dados[$this->PRESSAO_ARTERIAL_MINIMA]);
			#FREQUÊNCIA CARDÍACA
			array_push($colunas, $this->FREQUENCIA_CARDIACA);
			array_push($valores, $this->dados[$this->FREQUENCIA_CARDIACA]);
			#OBSERVAÇÕES
			array_push($colunas, $this->OBSERVACOES);
			array_push($valores, "'".$this->dados[$this->OBSERVACOES]."'");
			
			$colunas = implode(",", $colunas);
			$valores = implode(",", $valores);
			
			$script = "INSERT INTO $this->NOME_TABELA($colunas) VALUES ($valores)";
			return $script;
		}
		else 
			return null;
	}

	function inserir(){
		try{
			require "funcoes/conexao.php";
			$script = $this->getScriptInsert();
			//print($script);
			$con->query($script);
			$con->close();
			return true;
		}
		catch(Exception $e){
			print($e. " - Não foi possível registrar o Histórico de Saúde");
		}
	}

	function atualizar(){
		try{
			require "funcoes/conexao.php";
			$script = $this->getScriptUpdate();
			//print($script);
			$con->query($script);
			$con->close();
			return true;
		}
		catch(Exception $e){
			print($e. " - Não foi possível registrar o Histórico de Saúde");
		}
	}

	function estaFeito(){
		# Verifica se para esta avaliação, já houve um registro do tipo AVALIAÇÃO - MOTIVAÇÃO
		try{
			require "funcoes/conexao.php";
			$consulta = $con->query("SELECT id_avaliacao_id FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			$con->close();
			if($consulta->num_rows > 0)
				return true;
			else
				return false;
		}
		catch(Exception $e){
			print("Não foi possível verificar registros!");
		}
	}

	function buscar_registros(){
		# Busca registros do tipo AVALIAÇÃO - MOTIVAÇÃO - GRAU DE MOTIVAÇÃO
		try{
			require "funcoes/conexao.php";
			$consulta = $con->query("SELECT * FROM $this->NOME_TABELA WHERE
				id_avaliacao_id = $this->avaliacao");
			$con->close();
			if($consulta->num_rows > 0){
				while($registros = $consulta->fetch_assoc()){
					return $registros;
				}
			}
			return null;
		}
		catch(Exception $e){
			print("Não foi possível buscar os registros!");
			return null;
		}
	}

}