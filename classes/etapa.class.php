<?php

class EtapaAvaliacao{

	private $avaliacao;

	function __construct($id_avaliacao){
		$this->avaliacao = $id_avaliacao;
	}

	function getAvaliacaoId(){
		return $this->avaliacao;
	}

	function verificaDuplicidade(){
		try{
			require "funcoes/conexao.php";
			$busca = $con->query("SELECT id_avaliacao_id FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			$con->close();
			if($busca->num_rows == 0)
				return true;
			else
				return false;
		}
		catch(Exception $e){
			print($e);
			return null;
		}
	}

	function getDados(){
		try{
			require "funcoes/conexao.php";
			$busca = $con->query("SELECT * FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			if($busca->num_rows==1){
				$dados = $busca->fetch_assoc();
				return $dados;
			}
			else{
				return null;
			}
		}
		catch(Exception $e){
			print($e);
			return null;
		}
	}

}