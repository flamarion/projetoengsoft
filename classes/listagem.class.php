<?php

require 'funcoes/formatacao.php';

class ListagemClientes{

	private $conexao;
	private $usuario;
	private $filtro = "";

	function __construct($con, $id_usuario){
		$this->conexao = $con;
		$this->usuario = $id_usuario;
	}

	function getQuery(){
		$query = "SELECT pessoa.nome, pessoa.data_nascimento, pessoa.email, cliente.id_cliente FROM pessoa, cliente, cliente_usuario 
		WHERE pessoa.id_pessoa = cliente.id_pessoa_id 
		AND cliente_usuario.id_usuario_id = $this->usuario 
		AND cliente_usuario.id_cliente_id = cliente.id_cliente
		AND pessoa.nome LIKE '%".$this->filtro."%'";
		return $query;
	}

	function adicionarFiltro($filtro){
		$this->filtro = $filtro;
	}

	function busca(){
		$query = $this->getQuery();
		$busca = $this->conexao->query($query);
		return $busca;
	}

	function getLista(){
		$resultados = $this->busca();
		while($cliente = $resultados->fetch_assoc()){
			echo '<section class="item">
				<section class="right floated content">
					<a href="cliente.php?id='.$cliente["id_cliente"].'">
						<button class="ui blue button"><i class="user icon"></i>Perfil</button>
					</a>
					<a href="avaliacao_iniciar.php?cliente='.$cliente["id_cliente"].'">
						<button class="ui right labeled icon basic green button"><i class="flag icon"></i>Avaliar</button>
					</a>
				</section>
				<i class="user icon"></i>
				<section class="content">
					<span class="header">'.$cliente["nome"].'</span>
					'.idade($cliente['data_nascimento']).' anos, <i>'.$cliente["email"].'</i>
				</section>
			</section>';
		}
	}


}