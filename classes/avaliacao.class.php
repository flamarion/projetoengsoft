<?php 

class Avaliacao{

	private $NOME_TABELA = "avaliacao";

	private $id_usuario;


	function __construct(){}

	function setIdUsuario($id_usuario){
		$this->id_usuario = $id_usuario;
	}

	function listar($con){
		$resultados = array();
		//$query = "SELECT avaliacao.*, status_avalicao.*, cliente.nome FROM avaliacao INNER JOIN cliente ON (id_cliente = id_cliente_id) INNER JOIN status_avaliacao ON (id_status_avaliacao_id = id_status_avaliacao) WHERE id_usuario_id = $this->id_usuario";
		$query = "SELECT * FROM avaliacao INNER JOIN cliente ON id_cliente_id = id_cliente INNER JOIN pessoa ON id_pessoa_id = id_pessoa WHERE id_usuario_id = $this->id_usuario ORDER BY id_avaliacao DESC";
		try{
			$busca = $con->query($query);
			if($busca->num_rows > 0)
			{
				while($avaliacao = $busca->fetch_assoc())
					array_push($resultados, $avaliacao);
			}
		}
		catch(Exception $e){}
		return $resultados;
	}

	function concluir($con, $id_avaliacao){
		try{
			$script = "UPDATE avaliacao SET id_status_avaliacao_id = 2 WHERE id_avaliacao = $id_avaliacao";
			$con->query($script);
			session_destroy($_SESSION["avaliacao"]);
			return true;
		}
		catch(Exception $e)
		{
			return false;	
		}
		
	}


	function iniciar($usuario, $cliente){
		try{
			require "funcoes/conexao.php";
			$status_avalicao = 1;
			$query = "INSERT INTO avaliacao (id_usuario_id, id_cliente_id, data, hora, id_status_avaliacao_id) VALUES($usuario, $cliente, NOW(), NOW(), $status_avalicao)";
			$con->query($query);
			/* captura id da avaliação criada */
			$id_avaliacao = $con->insert_id;
			$this->criarSessao($id_avaliacao, $cliente);
			/*fechar conexão */
			$con->close();
			return true;
		}
		catch(Exception $e){
			return false;
		}
	}

	function criarSessao($avaliacao, $cliente){
		try{
			$_SESSION["avaliacao"] = array(
					"id" => $avaliacao,
					"cliente" => $cliente
				);
		}
		catch(Exception $e){}
	}

	function reiniciar($avaliacao, $usuario){
		try{
			require "funcoes/conexao.php";

			$busca = $con->query("SELECT id_cliente_id FROM avaliacao WHERE id_avaliacao = $avaliacao AND id_usuario_id = $usuario AND id_status_avaliacao_id IN (SELECT id_status_avaliacao FROM status_avaliacao WHERE descricao_status = 'aberta')");
			if($busca->num_rows == 1){
				$dados = $busca->fetch_assoc();
				$this->criarSessao($avaliacao, $dados["id_cliente_id"]);
				$con->close();
				return true;
			}
			else{
				$con->close();
				return false;
			}
		}
		catch(Exception $e){
			return false;
		}
	}
}


class VerificaAvaliacoes{

	private $usuario;
	private $quantidade = 0;

	function __construct($usuario){
		$this->usuario = $usuario;
	}

	function verificacao($con){
		try{
			$busca = $con->query("SELECT count(id_avaliacao) as quantidade FROM avaliacao 
				WHERE id_usuario_id = $this->usuario AND id_status_avaliacao_id IN 
					(SELECT id_status_avaliacao FROM status_avaliacao WHERE descricao_status = 'aberta')");

			$resultado = $busca->fetch_assoc();
			if($resultado["quantidade"] > 0){
				$this->quantidade = $resultado["quantidade"];
				return true;
			}
			return false;
		}
		catch(Exception $e){
			return false;
		}
	}

	function getQuantidade(){
		return $this->quantidade;
	}
}

