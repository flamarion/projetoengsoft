<?php

class Mensagem{

	/* 
	Essa classe tem por objetivo retornar um trecho de linguagem de marcação (HTML)
	que fornecerá uma mensagem para o usuário. Essa mensagem é baseada na interface do Semantic UI.

	1. Variáveis que compõe

		1.1 Tipo-> É recebido na função construtura. Identifica o tipo da mensagem à ser exibida
				- Codigo 0 (false): ERRO
				- Código 1 (true): SUCESSO
				- Código 2: ATENÇÃO
				- Código default - mensagem sem tipagem

		1.2 Titulo-> Título da mensagem que será exibida. String que representa o título é recebida na função
			construtura. Caso não haja o título, será emitido a palavra "Mensagem" como default.

		1.3 Conteúdo-> Lista de conteúdos ou linhas (itens de um array), que representam a mensagem em sí.
			A entrada desses itens é feita pela função addMensagem.

	*/

	private $tipo;
	private $titulo = "Mensagem!";
	private $conteudo = array();

	function __construct($tipo, $titulo){
		$this->tipo = $this->getMensagemTipo($tipo);
		if($titulo != "")
			$this->titulo = $titulo;
	}

	function addMensagem($item){
		array_push($this->conteudo, $item);
	}

	function getMensagemTipo($tipo){
		switch ($tipo) {
			case 0:
				return "error";
			case 1:
				return "success";
			case 2:
				return "warning";
			default:
				return "";
		}
	}

	function criaMensagem(){
		$mensagem = '<section class="ui '.$this->tipo.' message">
			<section class="header">'.$this->titulo.'</section>
				<ul class="list">'; /* cabeçalho */
				foreach ($this->conteudo as $linha) {
					$mensagem .= '<li>'.$linha.'</li>'; /* conteúdo */
				}
		$mensagem .= '</ul></section>'; /* final mensagem */
		return $mensagem;
	}

	function getMensagem(){
		print($this->criaMensagem());
	}


}