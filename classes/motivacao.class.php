<?php

//require "classes/etapa.class.php";

class MotivacaoAtividadeFisica{

	private $avaliacao;

	function __construct($avaliacao){
		$this->avaliacao = $avaliacao;
	}

	function inserir($conexao, $motivacao, $grau){
		# Insere um nova combinação AVALIAÇÃO - MOTIVAÇÃO - GRAU DE IMPORTÂNCIA DA MOTIVAÇÃO
		try{
			require "funcoes/conexao.php";
			$conexao->query("INSERT INTO avaliacao_motivacao VALUES($this->avaliacao, $motivacao, $grau)");
		}	
		catch(Exception $e){
			print("Não foi possível inserir!");
		}
	}

	function atualizar($conexao, $motivacao, $grau){
		# Atualiza um registro de combinação AVALIAÇÃO - MOTIVAÇÃO - GRAU DE IMPORTÂNCIA DA MOTIVAÇÃO
		try{
			$conexao->query("UPDATE avaliacao_motivacao SET id_grau_motivacao_id = $grau WHERE id_avaliacao_id = $this->avaliacao AND id_motivacao_id = $motivacao");
		}	
		catch(Exception $e){
			print("Não foi possível atualizar!");
		}	
	}

	function estaFeito($con){
		# Verifica se para esta avaliação, já houve um registro do tipo AVALIAÇÃO - MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT id_avaliacao_id FROM avaliacao_motivacao WHERE id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows > 0)
				return true;
			else
				return false;
		}
		catch(Exception $e){
			print("Não foi possível verificar registros!");
		}
	}

	function buscar_registros($con){
		# Busca registros do tipo AVALIAÇÃO - MOTIVAÇÃO - GRAU DE MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT * FROM avaliacao_motivacao, motivacao, grau_motivacao WHERE
				id_avaliacao_id = $this->avaliacao AND id_motivacao_id = id_motivacao AND
				id_grau_motivacao_id = id_grau_motivacao ORDER BY id_motivacao ASC");
			return $consulta;
		}
		catch(Exception $e){
			print("Não foi possível buscar os registros!");
		}
	}

}