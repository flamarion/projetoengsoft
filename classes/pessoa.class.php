<?php

class Pessoa{

	private $NOME_TABELA = "pessoa";

	private $id_pessoa;
	private $dados;

	private $NOME = "nome";
	private $CPF = "cpf";
	private $RG = "rg";
	private $EMAIL = "email";
	private $DATA_NASCIMENTO = "data_nascimento";
	private $GENERO = "id_genero_id";

	function setDados($dados){
		$this->dados = $dados;
	}

	function setIdPessoa($id_pessoa){
		$this->id_pessoa = $id_pessoa;
	}

	function getIdPessoa(){
		return $this->id_pessoa;
	}

	function converterData($data){
		return implode('-', array_reverse(explode('/', $_POST["data_nascimento"])));
	}

	function getScriptInsert(){
		if($this->dados != null){
			$colunas = array();
			$valores = array();

			array_push($colunas, $this->NOME);
			array_push($valores, "'".$this->dados[$this->NOME]."'");
			array_push($colunas, $this->CPF);
			array_push($valores, "'".$this->dados[$this->CPF]."'");
			array_push($colunas, $this->RG);
			array_push($valores, "'".$this->dados[$this->RG]."'");
			array_push($colunas, $this->EMAIL);
			array_push($valores, "'".$this->dados[$this->EMAIL]."'");
			array_push($colunas, $this->DATA_NASCIMENTO);
			array_push($valores, "'".$this->converterData($this->dados[$this->DATA_NASCIMENTO])."'");
			array_push($colunas, $this->GENERO);
			array_push($valores, $this->dados[$this->GENERO]);

			$colunas = implode(", ", $colunas);
			$valores = implode(", ", $valores);
			$query = "INSERT INTO $this->NOME_TABELA ($colunas) VALUES ($valores)";

			return $query;
		}
		print("<Pessoa> Não foi possível formular o script!");
		return null;
	}

	function inserir($con){
		try{
			$query = $this->getScriptInsert();
			if($query != null){
				//print($query);
				$con->query($query);
				$this->id_pessoa = $con->insert_id;
				return true;
			}
		}
		catch(Exception $e){}
		print("Houve um erro!");
		return false;
	}

	function busca($con){
		try{
			$busca = $con->query("SELECT * FROM $this->NOME_TABELA INNER JOIN genero ON (id_genero_id = id_genero) WHERE id_pessoa = $this->id_pessoa");
			if($busca->num_rows == 1){
				$pessoa = $busca->fetch_assoc();
				return $pessoa;
			}
		}
		catch(Exception $e){}
		return null;
	}

	function idade($data)
	{
		if($data != null && $data != "")
		{
			$array_data = explode("-", $data);
			$dia = (int) $array_data[2];
			$mes = (int) $array_data[1];
			$ano = (int) $array_data[0];
			
			date_default_timezone_set('America/Sao_Paulo');
			$atual = getdate();

			$dia_atual = (int) $atual["mday"];
			$mes_atual = (int) $atual["mon"];
			$ano_atual = (int) $atual["year"];

			$idade = $ano_atual - $ano;
			if($mes_atual < $mes)
				return $idade - 1;
			else if($mes_atual == $mes)
				if($dia_atual < $dia)
					return $idade - 1;
				else
					return $idade;
			else
				return $idade;
		}
		return null;
	}

}