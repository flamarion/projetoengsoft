<?php

require "classes/etapa.class.php";

class Perimetria extends EtapaAvaliacao{

	private $avaliacao;
	private $dados;
	
	public $NOME_TABELA = "perimetria";

	private $PESCOCO = "pescoco";
	private $OMBROS = "ombros";
	private $TORAX = "torax";
	private $ABDOMEN = "abdomen";
	private $CINTURA = "cintura";
	private $QUADRIL = "quadril";
	private $BRACO_ESQUERDO = "braco_esquerdo";
	private $BRACO_DIREITO = "braco_direito";
	private $ANTEBRACO_ESQUERDO = "antebraco_esquerdo";
	private $ANTEBRACO_DIREITO = "antebraco_direito";
	private $COXA_ESQUERDA = "coxa_esquerda";
	private $COXA_DIREITA = "coxa_direita";
	private $PANTURRILHA_ESQUERDA = "panturrilha_esquerda";
	private $PANTURRILHA_DIREITA = "panturrilha_direita";


	function __construct($avaliacao){
		$this->avaliacao = $avaliacao;
	}

	function setDados($dados){
		$this->dados = $dados;
	}

	function estaFeito($con){
		# Verifica se para esta avaliação, já houve um registro do tipo AVALIAÇÃO - MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT id_avaliacao_id FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows > 0)
				return true;
			else
				return false;
		}
		catch(Exception $e){
			print("Não foi possível verificar registros!");
		}
	}

	function buscar_registros($con){
		# Busca registros do tipo AVALIAÇÃO - MOTIVAÇÃO - GRAU DE MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT * FROM $this->NOME_TABELA WHERE
				id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows > 0){
				while($registros = $consulta->fetch_assoc()){
					return $registros;
				}
			}
			return null;
		}
		catch(Exception $e){
			print("Não foi possível buscar os registros!");
			return null;
		}
	}

	function inserir($con){
		if($this->dados != null){
			try{	
				$script = $this->getScriptInsert();
				$con->query($script);
				return true;
			}
			catch(Exception $e){
				return false;
			}
		}
		else{
			print("Não existem dados para serem inseridos!");
			return false;
		}
	}

	private function getScriptInsert(){
		if($this->dados != null){
			$colunas = array();
			$valores = array();

			array_push($colunas, "id_avaliacao_id");
			array_push($valores, $this->avaliacao);
			#PESCOCO
			array_push($colunas, $this->PESCOCO);
			array_push($valores, $this->dados[$this->PESCOCO]);
			#OMBROS
			array_push($colunas, $this->OMBROS);
			array_push($valores, $this->dados[$this->OMBROS]);
			#TORAX
			array_push($colunas, $this->TORAX);
			array_push($valores, $this->dados[$this->TORAX]);
			#ABDOMEN
			array_push($colunas, $this->ABDOMEN);
			array_push($valores, $this->dados[$this->ABDOMEN]);
			#CINTURA
			array_push($colunas, $this->CINTURA);
			array_push($valores, $this->dados[$this->CINTURA]);
			#QUADRIL
			array_push($colunas, $this->QUADRIL);
			array_push($valores, $this->dados[$this->QUADRIL]);
			#BRAÇO
			array_push($colunas, $this->BRACO_DIREITO);
			array_push($valores, $this->dados[$this->BRACO_DIREITO]);
			array_push($colunas, $this->BRACO_ESQUERDO);
			array_push($valores, $this->dados[$this->BRACO_ESQUERDO]);
			#ANTE-BRAÇO
			array_push($colunas, $this->ANTEBRACO_DIREITO);
			array_push($valores, $this->dados[$this->ANTEBRACO_DIREITO]);
			array_push($colunas, $this->ANTEBRACO_ESQUERDO);
			array_push($valores, $this->dados[$this->ANTEBRACO_ESQUERDO]);
			#COXA
			array_push($colunas, $this->COXA_DIREITA);
			array_push($valores, $this->dados[$this->COXA_DIREITA]);
			array_push($colunas, $this->COXA_ESQUERDA);
			array_push($valores, $this->dados[$this->COXA_ESQUERDA]);
			#PANTURRILHA
			array_push($colunas, $this->PANTURRILHA_DIREITA);
			array_push($valores, $this->dados[$this->PANTURRILHA_DIREITA]);
			array_push($colunas, $this->PANTURRILHA_ESQUERDA);
			array_push($valores, $this->dados[$this->PANTURRILHA_ESQUERDA]);

			$colunas = implode(", ", $colunas);
			$valores = implode(", ", $valores);

			$script = "INSERT INTO $this->NOME_TABELA ($colunas) VALUES($valores)";
			return $script;
		}
		return null;
	}

	function atualizar($con){
		if($this->dados != null){
			try{	
				$script = $this->getScriptUpdate();
				$con->query($script);
				return true;
			}
			catch(Exception $e){
				return false;
			}
		}
		else
			return false;
	}

	private function getScriptUpdate(){
		if($this->dados!=null){
			$atualizacoes = array();

			array_push($atualizacoes, $this->PESCOCO." = ".$this->dados[$this->PESCOCO]);
			array_push($atualizacoes, $this->OMBROS." = ".$this->dados[$this->OMBROS]);
			array_push($atualizacoes, $this->TORAX." = ".$this->dados[$this->TORAX]);
			array_push($atualizacoes, $this->ABDOMEN." = ".$this->dados[$this->ABDOMEN]);
			array_push($atualizacoes, $this->CINTURA." = ".$this->dados[$this->CINTURA]);
			array_push($atualizacoes, $this->QUADRIL." = ".$this->dados[$this->QUADRIL]);
			array_push($atualizacoes, $this->BRACO_ESQUERDO." = ".$this->dados[$this->BRACO_ESQUERDO]);
			array_push($atualizacoes, $this->BRACO_DIREITO." = ".$this->dados[$this->BRACO_DIREITO]);
			array_push($atualizacoes, $this->ANTEBRACO_ESQUERDO." = ".$this->dados[$this->ANTEBRACO_ESQUERDO]);
			array_push($atualizacoes, $this->ANTEBRACO_DIREITO." = ".$this->dados[$this->ANTEBRACO_DIREITO]);
			array_push($atualizacoes, $this->COXA_ESQUERDA." = ".$this->dados[$this->COXA_ESQUERDA]);
			array_push($atualizacoes, $this->COXA_DIREITA." = ".$this->dados[$this->COXA_DIREITA]);
			array_push($atualizacoes, $this->PANTURRILHA_ESQUERDA." = ".$this->dados[$this->PANTURRILHA_ESQUERDA]);
			array_push($atualizacoes, $this->PANTURRILHA_DIREITA." = ".$this->dados[$this->PANTURRILHA_DIREITA]);

			$atualizacoes = implode(", ",$atualizacoes);

			$script = "UPDATE $this->NOME_TABELA SET $atualizacoes WHERE id_avaliacao_id = $this->avaliacao";
			return $script;
		}
		return null;
	}

}