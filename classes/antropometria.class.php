<?php

//require "classes/etapa.class.php";

//class Antropometria extends EtapaAvaliacao{
class Antropometria{

	private $avaliacao;
	private $dados;
	public $NOME_TABELA = "antropometria";

	private $ID_PROTOCOLO_ANTROPOMETRICO_ID = "id_protocolo_antropometrico_id";
	private $ALTURA = "altura";
	private $PESO = "peso";
	private $CONDICIONAMENTO = "condicionamento";
	private $DENSIDADE_CORPORAL = "densidade_corporal";
	private $MASSA_MAGRA = "massa_magra";
	private $MASSA_GORDA = "massa_gorda";
	private $PERCENTUAL_DE_GORDURA = "percentual_de_gordura";
	private $PESO_MAXIMO_RECOMENDAVEL = "peso_maximo_recomendavel";
	private $OBJETIVO_DE_EMAGRECIMENTO = "objetivo_de_emagrecimento";
	private $MEDIDAS = array(
			"sb" => null,
			"tr" => null,
			"pe" => null,
			"am" => null,
			"si" => null,
			"ab" => null,
			"cx" => null
		);
	private $OBSERVACOES = "observacoes";

	public function __construct($avaliacao){
		//parent::__construct($id_avaliacao);
		$this->avaliacao = $avaliacao;
	}

	public function setDados($dados){
		$this->dados = $dados;
	}

	private function verificarProtocolo($con){
		try{
			$id = $this->dados["protocolo_utilizado"];
			$protocolo = $con->query("SELECT * FROM protocolo_antropometrico WHERE id_protocolo = $id LIMIT 1");
			$protocolo = $protocolo->fetch_assoc();
			$dobras = explode(";", $protocolo["dobras_medidas"]);
			$this->atribuirMedidasDobras($dobras);
		}
		catch(Exception $e){
			print("Não foi possível verificar o protocolo!");
		}
	}

	private function atribuirMedidasDobras($medidas_dobras_utilizadas){
		foreach ($this->medidaDobras as $dobra => $valor) {
			if(in_array($dobra, $medidas_dobras_utilizadas)){
				$this->medidaDobras[$dobra] = number_format((float) $this->dados["media_".$dobra], 3, '.', '');
			}
		}
	}


	function inserir($con){
		if($this->dados != null){
			try{
				$script = $this->getScriptInsert();
				$con->query($script);
				return true;
			}
			catch(Exception $e){
				print($e);
			}
		}
		return false;
	}

	private function getScriptInsert(){
		try{
			if($this->dados != null){
				$colunas = array();
				$valores = array();

				#AVALIACAO
				array_push($colunas, "id_avaliacao_id");
				array_push($valores, $this->avaliacao);
				#SOBRE O CLIENTE
				array_push($colunas, $this->ALTURA);
				array_push($valores, $this->dados[$this->ALTURA]);
				array_push($colunas, $this->PESO);
				array_push($valores, $this->dados[$this->PESO]);
				array_push($colunas, $this->CONDICIONAMENTO);
				array_push($valores, "'".$this->dados[$this->CONDICIONAMENTO]."'");
				#PROTOCOLO ANTROPOMÉTRICO
				array_push($colunas, $this->ID_PROTOCOLO_ANTROPOMETRICO_ID);
				array_push($valores, $this->dados[$this->ID_PROTOCOLO_ANTROPOMETRICO_ID]);

				#RESULTADOS
				array_push($colunas, $this->DENSIDADE_CORPORAL);
				array_push($valores, $this->dados[$this->DENSIDADE_CORPORAL]);
				array_push($colunas, $this->MASSA_MAGRA);
				array_push($valores, $this->dados[$this->MASSA_MAGRA]);
				array_push($colunas, $this->MASSA_GORDA);
				array_push($valores, $this->dados[$this->MASSA_GORDA]);
				array_push($colunas, $this->PERCENTUAL_DE_GORDURA);
				array_push($valores, $this->dados[$this->PERCENTUAL_DE_GORDURA]);
				array_push($colunas, $this->PESO_MAXIMO_RECOMENDAVEL);
				array_push($valores, $this->dados[$this->PESO_MAXIMO_RECOMENDAVEL]);
				array_push($colunas, $this->OBJETIVO_DE_EMAGRECIMENTO);
				array_push($valores, $this->dados[$this->OBJETIVO_DE_EMAGRECIMENTO]);

				foreach ($this->MEDIDAS as $medida => $valor) {
					$valor_medida = number_format((float) $this->dados[$medida."_medida_media"], 3, '.', '');
					array_push($colunas, "medida_".$medida);
					array_push($valores, $valor_medida);
				}

				array_push($colunas, $this->OBSERVACOES);
				array_push($valores, "'".$this->dados[$this->OBSERVACOES]."'");

				$colunas = implode(", ", $colunas);
				$valores = implode(", ", $valores);

				$script = "INSERT INTO $this->NOME_TABELA($colunas) VALUES($valores)";
				return $script;
			}
		}
		catch(Exception $e){}
		return null;
	}

	function atualizar($con){
		if($this->dados != null){
			try{
				$script = $this->getScriptUpdate();
				$con->query($script);
				return true;
			}
			catch(Exception $e){
				print($e);
			}
		}
		return false;
	}

	private function getScriptUpdate(){
		try{
			$atualizacoes = array();

			array_push($atualizacoes, $this->ALTURA." = ".$this->dados[$this->ALTURA]);
			array_push($atualizacoes, $this->PESO." = ".$this->dados[$this->PESO]);
			array_push($atualizacoes, $this->CONDICIONAMENTO." = '".$this->dados[$this->CONDICIONAMENTO]."'");
			array_push($atualizacoes, $this->ID_PROTOCOLO_ANTROPOMETRICO_ID." = ".$this->dados[$this->ID_PROTOCOLO_ANTROPOMETRICO_ID]);

			array_push($atualizacoes, $this->DENSIDADE_CORPORAL." = ".$this->dados[$this->DENSIDADE_CORPORAL]);
			array_push($atualizacoes, $this->MASSA_MAGRA." = ".$this->dados[$this->MASSA_MAGRA]);
			array_push($atualizacoes, $this->MASSA_GORDA." = ".$this->dados[$this->MASSA_GORDA]);
			array_push($atualizacoes, $this->PERCENTUAL_DE_GORDURA." = ".$this->dados[$this->PERCENTUAL_DE_GORDURA]);
			array_push($atualizacoes, $this->PESO_MAXIMO_RECOMENDAVEL." = ".$this->dados[$this->PESO_MAXIMO_RECOMENDAVEL]);
			array_push($atualizacoes, $this->OBJETIVO_DE_EMAGRECIMENTO." = ".$this->dados[$this->OBJETIVO_DE_EMAGRECIMENTO]);

			foreach ($this->MEDIDAS as $medida => $valor) {
				$valor_medida = number_format((float) $this->dados[$medida."_medida_media"], 3, '.', '');
				array_push($atualizacoes, "medida_".$medida." = ".$valor_medida);
			}

			array_push($atualizacoes, $this->OBSERVACOES." = '".$this->dados[$this->OBSERVACOES]."'");

			$atualizacoes = implode(", ", $atualizacoes);
			$script = "UPDATE $this->NOME_TABELA SET $atualizacoes WHERE id_avaliacao_id = $this->avaliacao";
			return $script;
		}
		catch(Exception $e){
			return false;
		}
	}

	function estaFeito($con){
		# Verifica se para esta avaliação, já houve um registro do tipo AVALIAÇÃO - MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT id_avaliacao_id FROM $this->NOME_TABELA WHERE id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows > 0)
				return true;
			else
				return false;
		}
		catch(Exception $e){
			print("Não foi possível verificar registros!");
		}
	}

	function buscar_registros($con){
		# Busca registros do tipo AVALIAÇÃO - MOTIVAÇÃO - GRAU DE MOTIVAÇÃO
		try{
			$consulta = $con->query("SELECT * FROM $this->NOME_TABELA WHERE
				id_avaliacao_id = $this->avaliacao");
			if($consulta->num_rows > 0){
				while($registros = $consulta->fetch_assoc())
					return $registros;
			}
			return null;
		}
		catch(Exception $e){
			print("Não foi possível buscar os registros!");
			return null;
		}
	}

}