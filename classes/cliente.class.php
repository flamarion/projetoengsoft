<?php

class Cliente{

	private $NOME_TABELA = "cliente";

	private $dados;
	private $id_usuario;
	private $id_pessoa;
	private $id_cliente;

	private $PESSOA = "id_pessoa_id";
	private $ESTADO_CIVIL = "id_estado_civil_id";
	private $ETNIA = "id_etnia_id";
	private $OCUPACAO = "ocupacao";
	private $CELULAR = "numero_celular";
	private $TELEFONE = "numero_telefone";

	function setIdCliente($id_cliente){
		$this->id_cliente = $id_cliente;
	}

	function setIdUsuario($id_usuario){
		$this->id_usuario = $id_usuario;
	}

	function setDados($dados){
		$this->dados = $dados;
	}

	function setIdPessoa($id_pessoa){
		$this->id_pessoa = $id_pessoa;
	}

	function getIdCliente(){
		return $this->id_cliente;
	}

	function getIdPessoa(){
		return $this->id_pessoa;
	}
	

	function getScriptInsert(){
		if($this->dados != null && $this->id_pessoa != null){
			$colunas = array();
			$valores = array();

			array_push($colunas, $this->PESSOA);
			array_push($valores, $this->id_pessoa);

			array_push($colunas, $this->ESTADO_CIVIL);
			array_push($valores, $this->dados[$this->ESTADO_CIVIL]);
			array_push($colunas, $this->ETNIA);
			array_push($valores, $this->dados[$this->ETNIA]);
			array_push($colunas, $this->CELULAR);
			array_push($valores, "'".$this->dados[$this->CELULAR]."'");
			array_push($colunas, $this->TELEFONE);
			array_push($valores, "'".$this->dados[$this->TELEFONE]."'");

			$colunas = implode(", ", $colunas);
			$valores = implode(", ", $valores);
			$query = "INSERT INTO $this->NOME_TABELA ($colunas) VALUES ($valores)";

			return $query;
		}
		print("<Cliente> Não foi possível formular o script!");
		return null;
	}

	function inserir($con){
		try{
			$query = $this->getScriptInsert();
			if($query != null){
				$con->query($query);
				$this->id_cliente = $con->insert_id;
				return true;
			}
		}
		catch(Exception $e){}
		return false;
	}

	function busca($con){
		try{
			$busca = $con->query("SELECT * FROM $this->NOME_TABELA INNER JOIN etnia ON (id_etnia_id = id_etnia) INNER JOIN  estado_civil ON (id_estado_civil_id = id_estado_civil) WHERE id_cliente = $this->id_cliente");
			if($busca->num_rows == 1){
				$cliente = $busca->fetch_assoc();
				return $cliente;
			}
		}
		catch(Exception $e)
		{}
		return null;
	}

}
