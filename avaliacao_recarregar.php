<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Painel";
		require "conf.php";
		require "funcoes/conexao.php";
		require "particoes/head.php";
		require "funcoes/verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require "particoes/header.php"; 
		?>
		<section class="row"> <!-- content !-->
			<section class="six wide column">

			<a href="avaliacoes_gerenciar.php" class="ui labeled icon basic red button"><i class="left arrow icon"></i> Voltar</a>
			<?php
				require "classes/mensagem.class.php";
				
				if(isset($_GET["id"])){
					unset($_SESSION["avaliacao"]);
					
					require "classes/avaliacao.class.php";

					$id_avaliacao = $_GET["id"];
					$id_usuario = $_SESSION["usuario"]["id_usuario"];

					$avaliacao = new Avaliacao();
					if($avaliacao->reiniciar($id_avaliacao, $id_usuario)){
						header("location:avaliacao.php");
					}
					else{
					 	$mensagem = new Mensagem(0, "Falhou!");
					 	$mensagem->addMensagem("Não foi possível abrir a avaliação!");
					 	$mensagem->getMensagem();	
					}
				}
				else{
					header("location:avaliacoes_gerenciar.php");
				}
				
			?>
			</section>
		</section>
		<?php
			require "particoes/footer.php";
		?>
</body>

</html>
