<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Avaliação Reiniciar";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";
	?>
</head>

<body>
	<section class="ui centered grid">
	<?php 
		require $header; 
		require $menu;
	?>
	<section class="row"> <!-- content !-->
		<section class="ten wide column">
		<?php
			if(isset($_GET["id"])){
				require $models."avaliacao.class.php";
				$avaliacao = new Avaliacao();
				$avaliacao->setId($_GET["id"]);
				$busca = $avaliacao->buscar($con);

				$etapas_realizadas = array();

				if($busca != null){
					require $models."motivacao_atividade_fisica.class.php";
					require $models."antropometria.class.php";
					require $models."amostra_saude.class.php";
					$id_avaliacao = $busca["id_avaliacao"];

					$motivacoes = new Motivacao_atividade_fisica($id_avaliacao, null, null);
					if($motivacoes->buscar($con)->num_rows > 0)
						array_push($etapas_realizadas, 'motivacao');

					$antropometria = new Antropometria();
					$antropometria->setAvaliacao($id_avaliacao);
					if($antropometria->buscar($con)->num_rows > 0)
						array_push($etapas_realizadas, 'antropometria');

					$amostra_saude = new Amostra_saude();
					$amostra_saude->setAvaliacao($id_avaliacao);
					if($amostra_saude->buscar($con)->num_rows > 0)
						array_push($etapas_realizadas, 'amostra');


					unset($_SESSION["avaliacao"]);
					$_SESSION["avaliacao"] = array(
							"id" => $id_avaliacao,
							"etapas" => $etapas_realizadas
						);
					header("location:avaliacao.php");
					//print_r($_SESSION["avaliacao"]);
				}
				else{
					print("Avaliação inexistente");
				}
			}
		?>
		</section>
	</section>
	<?php
		require $footer;
	?>
</body>

</html>
