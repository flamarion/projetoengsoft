<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Relatório da Avaliação";
		require "particoes/head.php";
		require "funcoes/verifica_sessao.php";

		if(isset($_GET["id"]))
			$id_avaliacao = $_GET["id"];
		else
			header("location:avaliacoes.php");
	?>
	<script type="text/javascript">
		$(document).ready(function(){
			// print();
		})
	</script>
</head>

<body>
	<section class="ui centered grid">
		<section class="row">
			<?php
				require "funcoes/conexao.php";
				require "classes/mensagem.class.php";

				$busca_avaliacao = $con->query("SELECT * FROM avaliacao WHERE id_avaliacao = $id_avaliacao");
				if($busca_avaliacao->num_rows > 0){
					require "classes/motivacao.class.php";
					require "classes/amostra.class.php";
					require "classes/historico.class.php";
					require "classes/antropometria.class.php";
					require "classes/perimetria.class.php";

					$avaliacao = $busca_avaliacao->fetch_assoc();
					$id_cliente = $avaliacao["id_cliente_id"];
					$id_usuario = $avaliacao["id_usuario_id"];

					$busca_cliente = $con->query("SELECT * FROM cliente INNER JOIN pessoa ON id_pessoa_id = id_pessoa INNER JOIN genero ON id_genero_id = id_genero INNER JOIN estado_civil ON id_estado_civil_id = id_estado_civil INNER JOIN etnia ON id_etnia_id = id_etnia WHERE id_cliente = $id_cliente");
					$cliente = $busca_cliente->fetch_assoc();

					$busca_usuario = $con->query("SELECT * FROM usuario INNER JOIN pessoa ON id_pessoa_id = id_pessoa WHERE id_usuario = $id_usuario");
					$usuario = $busca_usuario->fetch_assoc();

					$__motivacao = new MotivacaoAtividadeFisica($id_avaliacao);
					$__amostra = new AmostraDeSaude($id_avaliacao);
					$__historico = new HistoricoDeSaude($id_avaliacao);
					$__antropometria = new Antropometria($id_avaliacao);
					$__perimetria = new Perimetria($id_avaliacao);

					if($__motivacao->estaFeito($con))
						$motivacoes = $__motivacao->buscar_registros($con);
					if($__amostra->estaFeito($con))
						$amostra_de_saude = $__amostra->buscar_registros($con);
					if($__historico->estaFeito($con))
						$historico_de_saude = $__historico->buscar_registros($con);
					if($__antropometria->estaFeito($con))
						$antropometria = $__antropometria->buscar_registros($con);
					if($__perimetria->estaFeito($con))
						$perimetria = $__perimetria->buscar_registros($con);

					

				}
				$con->close();
			?>
			<section class="fifteen wide column">
				<section class="ui segment">
					<h2 class="ui centered header">
						Resultados da Avaliação
						<section class="sub header">
							<?php print('<strong>'.$avaliacao["hora"].' em '.$avaliacao["data"].'</strong>'); ?> - Joinville - Santa Catarina
							<br>
							<?php print($usuario["nome"]." <i>(".$usuario["email"].")</i>"); ?>
						</section>
					</h2>
				</section>
				
				<section class="ui dividing header"><i class="user icon"></i> Cliente</section>
				<table class="">
					<thead>
						<tr>
							<th colspan="2">Nome</th>
							<th>Gênero</th>
							<th>Data de Nascimento</th>
							<th>CPF</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2"><?php print($cliente["nome"]); ?></td>
							<td><?php print($cliente["descricao_genero"]); ?></td>
							<td><?php print($cliente["data_nascimento"]); ?></td>
							<td><?php print($cliente["cpf"]); ?></td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th>Etnia</th>
							<th>Estado Civil</th>
							<th>Email</th>
							<th>Telefone</th>
							<th>Celular</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php print($cliente["descricao_etnia"]); ?></td>
							<td><?php print($cliente["descricao_estado_civil"]); ?></td>
							<td><?php print($cliente["email"]); ?></td>
							<td><?php print($cliente["numero_telefone"]); ?></td>
							<td><?php print($cliente["numero_celular"]); ?></td>
						</tr>
					</tbody>
				</table>
			</section>
		</section>
	</section>
	<style type="text/css">
		table tr, table td, table th{
			border: 1px solid #000;
			padding: 6px 3px;
		}
		table{
			width: 100%;
		}
		table th{
			background-color: #e1e1e1;
		}
	</style>
</body>

</html>
