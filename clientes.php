<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Clientes";
		require "conf.php";
		require "conexao.php";
		require $head;
		require $control."verifica_sessao.php";

		$filtro = (isset($_GET['filtro']))? $_GET['filtro'] : "";
		$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
	?>
</head>

<body>
	<section class="ui centered grid">
		<?php 
			require $header; 
			require $menu;
		?>
		<section class="black row">
			<section class="ten wide column">
				<section class="ui inverted menu">
					<section class="item">
						<h2 class="ui inverted header">
							<i class="users icon"></i>
							<section class="content">
								Lista de Clientes
							</section>
						</h2>
					</section>
					<section class="right menu">
						<section class="item">
							<form method="POST" action="busca.php">
								<section class="ui left icon input">
									<input type="text" name="filtro" placeholder="Pesquisar..." value="<?php print($filtro); ?>">
									<i class="search icon"></i>
								</section>
								<input type="submit" class="ui medium submit orange button" value="Buscar">
							</form>
						</section>
					</section>
				</section>
			</section>
		</section>
		<section class="row"> <!-- content !-->
			<section class="ten wide column">
				<section class="ui segment">

					<section class="ui celled list">
					<?php
						require $models."cliente.class.php";
						require $classes."mensagem.class.php";
						require $control."data.php";
						require $control."cores.php";

						$clientes = new Cliente();
						$lista_clientes = $clientes->listar($con, $_SESSION["usuario"]["id"], $filtro, null, null);

						if($lista_clientes!=null){
							if($lista_clientes->num_rows > 0){
								$indice = 1;
								while($cliente = $lista_clientes->fetch_assoc()){
									$cor = trocaCor("blue", "teal", $indice);
									$indice += 1;

									print('<section class="item">
										<a href="cliente.php?id='.$cliente["id"].'">
											<button class="ui left floated icon '.$cor.' button"><i class="user icon"></i></button>
										</a>
										<section class="left floated content">
											<article class="header">'.$cliente["nome"].' '.$cliente["sobrenome"].'</article>
											'.idade($cliente["data_nascimento"]).' anos, '.$cliente["email"].' 
										</section>
										<section class="right floated content">
											<a href="avaliacao_iniciar.php?cliente='.$cliente["id"].'">
												<button class="ui '.$cor.' button">Avaliar</button>
											</a>
										</section>
									</section>');
								}
							}
							else{
								$mensagem = new Mensagem(2, "Sem clientes!");
								if($filtro == "")
									$mensagem->addMensagem("Não houveram resultados na sua busca por clientes!");
								else
									$mensagem->addMensagem("Sem resultados para a busca de <strong>$filtro</strong>!");
								$mensagem->getMensagem();
								print('<a href="cliente_adicionar.php" class="ui fluid blue button">Cadastrar cliente</a>');
							}
						}
					?>
					</section>
				</section>
			</section>
		</section>
		<?php
			require $footer;
		?>
</body>

</html>
