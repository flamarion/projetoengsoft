<?php

require $models."antropometria.class.php";
require $models."dobra_cutanea.class.php";

$dobra_cutanea = new Dobra_cutanea();
$dobras = $dobra_cutanea->listar($con);

$verifica_antropometria = new Antropometria();
$verifica_antropometria->setAvaliacao($_SESSION["avaliacao"]["id"]);
$preenchimento = $verifica_antropometria->estaPreenchido($con);

if(count($_POST) > 0){
	$nova_antropometria = new Antropometria();

	$nova_antropometria->setDados(
		$_SESSION["avaliacao"]["id"], 
		$_POST["altura"],
		$_POST["peso"]
	);

	if($preenchimento){
		$msg = new Mensagem(0, "Error!");
		$msg->addMensagem("Antropometria já está registrada!");
		$msg->getMensagem();
	}
	else{
		if($nova_antropometria->inserir($con))
		{
			$msg = new Mensagem(1, "Sucesso!");
			$msg->addMensagem("Antropometria registrada!");
			$msg->getMensagem();

			while ($dobra = $dobras->fetch_assoc()) {
				$valor = $_POST[$dobra["sigla_dobra"]."_medida"];
				$nova_antropometria->registrarMedidaDobra($con, $dobra["id_dobra_cutanea"], $valor);
			}

			array_push($_SESSION["avaliacao"]["etapas"], $etapa_atual);
		}
	}
}

if($preenchimento){
	$antropometria = $verifica_antropometria->buscar($con);
	$dados_antropometria = $antropometria->fetch_assoc();
	
	print("<input type='hidden' id='preenchimento' value='1'>");
	foreach ($dados_antropometria as $chave => $valor) {
		print('<input type="hidden" id="valor_'.$chave.'" value="'.$valor.'">');
	}

	$sql = "SELECT sigla_dobra, valor_medida 
			FROM medida_dobra INNER JOIN dobra_cutanea ON (id_dobra_cutanea_id = id_dobra_cutanea) 
			WHERE id_antropometria_id = ".$dados_antropometria["id_antropometria"];
	$medidas_dobras = $con->query($sql);

	while($medida_dobra = $medidas_dobras->fetch_assoc())
		print('<input type="hidden" id="valor_'.$medida_dobra["sigla_dobra"].'" value="'.$medida_dobra["valor_medida"].'">');	
}

?>
<form method="POST" action="avaliacao.php?etapa=<?php print($etapa) ?>" class="ui form">	
	<section class="ui red segment">
		<h2 class="ui header">
			<i class="edit icon"></i>
			<section class="content">
				Antropometria <i></i>
				<section class="sub header">
					Preencha o formulário antropométrico e calcule os resultados antes de prosseguir!
				</section>
			</section>
		</h2>

		<article class="ui dividing header">
			Insira as Medidas
		</article>

		<section class="fields">
			<section class="five wide required field">
				<label>Altura</label>
				<section class="ui right labeled input">
					<input type="text" name="altura">
					<span class="ui label">cm</span>
				</section>
			</section>

			<section class="five wide required field">
				<label>Peso</label>
				<section class="ui right labeled input">
					<input type="text" name="peso">
					<span class="ui label">kg</span>
				</section>
			</section>

			<section class="six wide required field">
				<label>Nível de Condicionamento</label>
				<select class="ui dropdown" name="condicionamento">
					<option value="">Selecione...</option>
					<option value="sedentário">Sedentário</option>
					<option value="ativo">Ativo</option>
					<option value="atleta">Atleta</option>
				</select>
			</section>
		</section>

		<section class="field">
			<label>Protocolo Antropométrico</label>
			<select class="ui dropdown" id="select_protocolo_antropometrico">
				<option value="">Selecione o protocolo...</option>
				<?php
					
				?>
			</select>

			<input type="hidden" name="id_protocolo_antropometrico_id" id="input_protocolo_antropometrico">
		</section>

		<table class="ui striped table" id="antropometria">

			<thead>
				<tr>
					<th></th>
					<th>1ª Medida</th>
					<th>2ª Medida</th>
					<th>3ª Medida</th>
					<th>Média</th>
				</tr>
			</thead>

			<tbody>
				<?php
					while($dobra = $dobras->fetch_assoc()){
						?>
						<tr>
							<td>
								<strong><?php print($dobra["desc_dobra_cutanea"]) ?></strong>
							</td>
							<td>
								<section class="ui input">
									<input type="text" id="<?php print($dobra["sigla_dobra"]) ?>_medida_1">
						 		</section>
						 	</td>
							<td>
						 		<section class="ui input">
									<input type="text" id="<?php print($dobra["sigla_dobra"]) ?>_medida_2">
						 		</section>
						 	</td>
							<td>
						 		<section class="ui input">
									<input type="text" id="<?php print($dobra["sigla_dobra"]) ?>_medida_3">
						 		</section>
							</td>
							<td>
								<section class="ui input">
									<input type="text" name="<?php print($dobra["sigla_dobra"]) ?>_medida" id="<?php print($dobra["sigla_dobra"]) ?>_medida_media" maxlenght="5" readonly>
								</section>
							</td>
						</tr>
						<?php
					}
				?>
			</tbody>
			
		</table>

		<section>
			<input type="button" class="ui right floated blue button" id="calcular" value="Calcular" disabled>
		</section>
	</section>
	<section class="ui segment">
		<article class="ui dividing header">
			Resultados da Antropometria
		</article>

		<section class="three fields">
			<section class="field">
				<label>Densidade Corporal</label>
				<section class="ui right labeled input">
					<input type="text" name="densidade_corporal" value="0" readonly="readonly">
					<label class="ui label">g/ml</label>
				</section>
			</section>
			<section class="field">
				<label>Massa Magra</label>
				<section class="ui right labeled input">
					<input type="text" name="massa_magra" value="0" readonly="readonly">
					<label class="ui label">kg</label>
				</section>
			</section>
			<section class="field">
				<label>Massa Gorda</label>
				<section class="ui right labeled input">
					<input type="text" name="massa_gorda" value="0" readonly="readonly">
					<label class="ui label">kg</label>
				</section>
			</section>
		</section>

		<section class="three fields">
			<section class="field">
				<label>Percentual de Gordura</label>
				<section class="ui right labeled input">
					<input type="text" name="percentual_de_gordura" value="0" readonly="readonly">
					<label class="ui label">%</label>
				</section>
			</section>
			<section class="field">
				<label>Peso Máx. Recomendável</label>
				<section class="ui right labeled input">
					<input type="text" name="peso_maximo_recomendavel" value="0" readonly="readonly">
					<label class="ui label">kg</label>
				</section>
			</section>
			<section class="field">
				<label>Objetivo de Emagrecimento</label>
				<section class="ui right labeled input">
					<input type="text" name="objetivo_de_emagrecimento" value="0" readonly="readonly">
					<label class="ui label">kg</label>
				</section>
			</section>
		</section>

		<section class="field">
			<label>Observações/Laudo</label>
			<textarea name="observacoes"></textarea>
		</section>

		<input type="reset" class="ui left floated orange button" value="Limpar">
		<input type="submit" class="ui right floated green submit button" value="Salvar">	
	</section>
</form>
<script type="text/javascript" src="static/js/antropometria.js"></script>

<?php
	/*if(isset($registros)){
		print("<input type='hidden' id='registros' value='true'>");
		foreach ($registros as $field => $valor)
			print("<input type='hidden' id='$field' value='$valor'>");
	}*/
?>