<?php

require $models."historico_saude.class.php";

if(count($_POST) > 0){
	
	$questoes = array(
			"consumo_alcool" => 0,
			"consumo_fumo" => 0,
			"problema_cardiovascular" => 0,
			"problema_cardiovascular_familiar" => 0,
			"problema_ortopedico" => 0,
			"consumo_medicamentos" => 0,
			"problema_menstrual" => 0
		);
	
	foreach ($_POST["historico"] as $valor) {
		if(array_key_exists($valor, $questoes))	
			$questoes[$valor] = 1;
	}

	$hist = new Historico_saude();

	

}

?>
<section class="ui green segment">
	<h2 class="ui header">
		<i class="heartbeat icon"></i>
		<section class="content">
			Histórico de Saúde <i></i>
			<section class="sub header">
				Essas informações podem ser fundamentais para análises futuras
			</section>
		</section>
	</h2>
	<script type="text/javascript" src="static/js/historico.js"></script>
	<form class="ui form" method="POST" action="avaliacao.php?etapa=<?php echo $etapa ?>">

		<table class="ui compact celled definition table">
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="consumo_alcool">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Consome bebidas alcóolicas
				</td>
			</tr>
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="consumo_fumo">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Fumante
				</td>
			</tr>
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="problema_cardiovascular">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Possui problema cardiovascular
				</td>
			</tr>
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="problema_cardiovascular_familiar">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Problema cardiovascular na família
				</td>
			</tr>
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="problema_ortopedico">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Lesões ou problemas ortopédicos
				</td>
			</tr>
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="consumo_medicamentos">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Faz o uso de medicamentos ou suplementos
				</td>
			</tr>
			<tr>
				<td class="collapsing">
					<section class="ui toggle checkbox">
						<input type="checkbox" name="historico[]" value="problema_menstrual">
						<label></label>
					</section>
				</td>
				<td>
					<!-- Motivo !-->
					Problemas no período menstrual ou disfunção menstrual
				</td>
			</tr>
		</table>

		<section class="three fields">
			<section class="field">
				<label>Pressão Arterial Máxima</label>
				<section class="ui right labeled input">
					<input type="text" name="pressao_arterial_maxima">
					<section class="ui label">
						mmHg
					</section>
				</section>
			</section>

			<section class="field">
				<label>Pressão Arterial Mínima</label>
				<section class="ui right labeled input">
					<input type="text" name="pressao_arterial_minima">
					<section class="ui label">
						mmHg
					</section>
				</section>
			</section>

			<section class="field">
				<label>Frequência cardíaca em repouso</label>
				<section class="ui right labeled input">
					<input type="text" maxlength="3" name="frequencia_cardiaca">
					<section class="ui label">
						bpm
					</section>
				</section>
			</section>
		</section>

		<section class="field">
			<label>Observações</label>
			<textarea name="observacoes"></textarea>
		</section>

		<input type="reset" class="ui left floated orange button" value="Limpar">

		<input type="submit" class="ui right floated green button" value="Salvar">
	</form>
</section>

<?php
/*
if(isset($registros)){
	foreach ($registros as $field => $valor) {
		print('<input type="hidden" value="'.$valor.'" id="'.$field.'">');
	}
}*/
?>