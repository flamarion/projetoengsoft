<section class="ui yellow segment">
	<h2 class="ui header">
		<i class="doctor icon"></i>
		<section class="content">
			Testes Finais
			<section class="sub header">
				Preencha os testes finais!
			</section>
		</section>
	</h2>
	<form method="POST" action="" class="ui form">
		<section class="ui segment">
			<?php
				require "formularios/vo2submaximo.html";
				require "formularios/flexibilidade.html";
				require "formularios/forca.html";
			?>
			<input type="reset" class="ui left floated button" value="Limpar">
			<input type="submit" class="ui right floated green submit button" value="Continuar">
		</section>
	</form>
</section>