<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		if(isset($_SESSION["usuario"]))
		 	header("location: painel.php");
		$titulo = "Início";
		require "conf.php";
		require $head;
	?>
</head>

<body class="background">
	<section class="ui centered grid">
		<?php require $header; ?>
		<section class="row">
			<section class="six wide column">
				<?php 
					if(count($_POST) > 0)
					{
						require $classes."log.class.php";
						require $classes."mensagem.class.php";
						require "conexao.php";

						$log = new LogIn($_POST["login"], $_POST["senha"]);
						if($log->autenticar($con)){
							$_SESSION["usuario"] = $log->getDadosUsuario();
							header("location:painel.php");
						}
						else{
							$mensagem = new Mensagem(0, "Houve um erro!");
							$mensagem->addMensagem($log->getErro());
							$mensagem->getMensagem();
						}
					}
					require $forms."login.html"; 
				?>
			</section>
		</section>
	</section>
</body>

</html>
