<!DOCTYPE html>
<html>
<head>
	<?php
		session_start();
		$titulo = "Clientes";
		require "conf.php";
		require "funcoes/conexao.php";
		require "particoes/head.php";
		require "funcoes/verifica_sessao.php";

		$filtro = (isset($_GET['filtro']))? $_GET['filtro'] : null;
		$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
	?>
</head>

<body>
	<div class="ui centered grid">
		<div class="black row">
			<?php
				require($templates."bases/topo_painel.php");
			?>
		</div>
		<div class="row">
			<section class="twelve wide column">
			<?php
				require($templates."nav/painel_menu.php");
			?>
			</section>
		</div>
		<div class="row">
			<div class="ten wide column">
				<section class="ui segment">

					<h2 class="ui header">
						<i class="users icon"></i>
						<section class="content">
							Lista de Clientes
							<section class="sub header">
								Para registrar mais clientes <a href="cliente_adicionar.php">clique aqui</a>
							</section>
						</section>
					</h2>

					<form method="POST" action="busca.php">
						<div class="ui action input">
							<input type="text" name="filtro" placeholder="Buscar..." value="<?php echo $filtro; ?>">
							<input type="submit" class="ui black button" value="Procurar">
						</div>
					</form>
				<?php
					require($funcoes."busca.php");
					require($funcoes."formatacao.php");

					$quantidade_registros = quantidadeRegistros($con, $id, $filtro);
					$clientes_pagina = clientesPorPagina();

					
					$busca = listaClientes($con, $id, $clientes_pagina, $pagina, $filtro);
					
					if($busca->num_rows > 0){
						$total_de_paginas = ceil($quantidade_registros/$clientes_pagina);
						echo '</br><span class="text">Página '.$pagina.' de '.$total_de_paginas.'</span>';
						
						require($templates."/bases/listagem_cliente_item.php");
						
						echo '<div class="ui celled list">';
						while($dados = $busca->fetch_assoc()){
							echo item($media,$dados);
						}
						echo "</div>";
					}else{
						echo '<article class="ui message">Nenhum cliente encontrado</article>';
					}

					echo "</br>";
					if($pagina > 1){
						echo '<a class="ui left floated tiny labeled icon black button" href="clientes.php?pagina='.($pagina-1).'&filtro='.$filtro.'">
							<i class="angle double left icon"></i>
							Anterior
						</a>';	
					}

					if($pagina < ($quantidade_registros/$clientes_pagina)){
						echo '<a class="ui right floated right labeled icon tiny black button" href="clientes.php?pagina='.($pagina+1).'&filtro='.$filtro.'">
							<i class="angle double right icon"></i>
							Próximo
						</a>';
					}

				?>
				</section>
			</div>
		</div>
		<footer class="blue row">
			<?php
				require($templates."bases/rodape.php");
				mysqli_close($con); 
			?>
		</footer>
</body>

</html>
