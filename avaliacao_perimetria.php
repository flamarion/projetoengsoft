<section class="ui teal segment">
	<h2 class="ui header">
		<i class="edit icon"></i>
		<section class="content">
			Perimetria
			<section class="sub header">
				Preencha o formulário com as medidas em centrímetros!
			</section>
		</section>
	</h2>
	<form method="POST" action="avaliacao.php?etapa=<?php print($etapa) ?>" class="ui form">
		<table class="ui striped table" id="perimetria">
			<thead>
				<tr>
					<th style="width: 40%!important;">Local da Medida</th>
					<th>Lado Direito</th>
					<th>Lado Esquerdo</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<strong>Pescoço</strong>
					</td>
					<td colspan="2">
						<section class="ui fluid right labeled input">
							<input type="text" name="pescoco" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Ombros</strong>
					</td>
					<td colspan="2">
						<section class="ui fluid right labeled input">
							<input type="text" name="ombros" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Tórax</strong>
					</td>
					<td colspan="2">
						<section class="ui fluid right labeled input">
							<input type="text" name="torax" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Braços</strong>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Direito" name="braco_direito" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Esquerdo" name="braco_esquerdo" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Antebraços</strong>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Direito" name="antebraco_direito" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Esquerdo" name="antebraco_esquerdo" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Abdômen</strong>
					</td>
					<td colspan="2">
						<section class="ui fluid right labeled input">
							<input type="text" name="abdomen" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Cintura</strong>
					</td>
					<td colspan="2">
						<section class="ui fluid right labeled input">
							<input type="text" name="cintura" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Quadril</strong>
					</td>
					<td colspan="2">
						<section class="ui fluid right labeled input">
							<input type="text" name="quadril" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Coxa</strong>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Direita" name="coxa_direita" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Esquerda" name="coxa_esquerda" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Panturrilha</strong>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Direita" name="panturrilha_direita" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
					<td>
						<section class="ui fluid right labeled input">
							<input type="text" placeholder="Esquerda" name="panturrilha_esquerda" required>
							<label class="ui  label">cm</label>
						</section>
					</td>
				</tr>
			</tbody>
		</table>

		<section>
			<input type="reset" class="ui left floated orange button" value="Limpar">
			<input type="submit" class="ui right floated green submit button" value="Salvar">	
		</section>
	</form>
</section>
<?php
if(isset($registros)){
	print("<section id='registros'>");
		foreach ($registros as $field => $valor) {
			print("<input type='hidden' id='$field' value='$valor'>");
		}
	print("</section>");
}
?>
<script type="text/javascript">
	$('#registros').ready(function(){
		fields = [
			"pescoco",
			"ombros",
			"torax",
			"abdomen",
			"cintura",
			"quadril",
			"braco_esquerdo",
			"braco_direito",
			"antebraco_esquerdo",
			"antebraco_direito",
			"coxa_esquerda",
			"coxa_direita",
			"panturrilha_esquerda",
			"panturrilha_direita"
		];

		for(var f in fields)
		{	
			field = fields[f];
			valor_atual = $('input[type="hidden"][id="'+field+'"]').val();
			$('input[name="'+field+'"]').val(valor_atual);
		}
	})
</script>